#!/bin/sh

SRC_DIR='/mnt/usb/clients'
DST_DIR='/usr/lib/lua/luci/view/admin_status'

TPL='index.tpl.htm'
DST='index.htm'

TBODY_OPEN='<tbody><tr><th>Identifikation</th><th>Aktivität</th><th>Aktion</th></tr>'
TR_OPEN='<tr>'
TR_CLOSE='</tr>'
TR_CLIENT=''
TBODY_CLOSE='</tbody>'
CLIENT_ROWS=''

if [ ! -e "$SRC_DIR" ]; then
	logger "$SRC_DIR does not exist. run 'sh clients.sh' first."	
	exit 0
fi

for QUERIES_FILE in `ls -S "$SRC_DIR"|grep queries`; do

	CLIENT_MAC=`echo "$QUERIES_FILE"|cut -c 1-17`
	CLIENT_MAC2=`echo "$CLIENT_MAC"|sed 's/-/:/g'`
	
	HOSTNAME=`cat "$SRC_DIR"/"$CLIENT_MAC"-meta.log|grep '+' -m 1|awk '{print $6}'`
	
	REQUEST_COUNT=`cat "$SRC_DIR/$QUERIES_FILE"|wc -l`

	# echo "$CLIENT_MAC"
	# echo "$REQUEST_COUNT"

	TD_NAME="<td width=\"33%\"><a href=\"/cgi-bin/luci/admin/status/client_domains?id=$CLIENT_MAC.html\">$HOSTNAME<br>$CLIENT_MAC</a></td>"
	TD_COUNT="<td>$REQUEST_COUNT</td>"
	TD_ACTION="<td><button class=\"btn btn-sm btn-primary\" value=\"0\" onclick=\"mac_action('$CLIENT_MAC2', this)\">Sperren</a></td>"
	
	TR_OPEN="<tr id=\"$CLIENT_MAC2\">"

	TR_CLIENT="$TR_CLIENT$TR_OPEN"
		
	TR_CLIENT="$TR_CLIENT$TD_NAME"
	TR_CLIENT="$TR_CLIENT$TD_COUNT"
	TR_CLIENT="$TR_CLIENT$TD_ACTION"
	TR_CLIENT="$TR_CLIENT$TR_CLOSE"

	CLIENT_ROWS="$CLIENT_ROWS$TR_CLIENT"

	TR_CLIENT=''
done

CLIENT_ROWS="$TBODY_OPEN$CLIENT_ROWS$TBODY_CLOSE"

sed "s|{{hotspot_client_index_rows}}|$CLIENT_ROWS|g" < "$DST_DIR/$TPL" > "$DST_DIR/$DST"

exit 0
