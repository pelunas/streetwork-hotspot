#!/bin/sh

# hosts file must contain user block markings

# run 
# sh /etc/hotspot/add-user-block-to-hosts.sh

# echo '# user-blocklist-start #' >> /etc/hosts
# echo '0.0.0.0 foo' >> /etc/hosts
# echo '# user-blocklist-end #' >> /etc/hosts


# HOSTS_FILE='/etc/hotspot/debug-hosts.txt'
HOSTS_FILE='/etc/hosts'
TEMP_FILE='/mnt/usb/hotspot/temp-hosts.txt'
USER_FILE='/mnt/usb/hotspot/user-blocked-hosts.txt'

DOMAIN_TO_BLOCK="$1"

# add domain to users hosts file 

echo "0.0.0.0 $DOMAIN_TO_BLOCK" >> "$USER_FILE"

# remove possible duplicates

awk '!a[$0]++' "$USER_FILE" > "$TEMP_FILE"

# rename it back

mv "$TEMP_FILE" "$USER_FILE"

# find user block indices in hosts file

user_block_start_index=`grep -n 'user-blocklist-start' < "$HOSTS_FILE" | cut -d ":" -f1`
user_block_end_index=`grep -n 'user-blocklist-end' < "$HOSTS_FILE" | cut -d ":" -f1`

user_block_first_line=`expr $user_block_start_index + 1`
user_block_last_line=`expr $user_block_end_index - 1`

# delete current user block from hosts file

sed -e "${user_block_first_line},${user_block_last_line}d" < "$HOSTS_FILE" > "$TEMP_FILE"

# insert new block from user file into hosts file

sed "/user-blocklist-start/r ${USER_FILE}" "$TEMP_FILE" > "$HOSTS_FILE"

# clean up

rm -f "$TEMP_FILE"

echo 0

exit 0