-- Lua implementation of PHP scandir function
-- plus grep for queries files 
function scandir(directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls "'..directory..'"'..'|grep queries')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

function split(str, pat, max, regex)
	pat = pat or "\n"
	max = max or #str

	local t = {}
	local c = 1

	if #str == 0 then
		return {""}
	end

	if #pat == 0 then
		return nil
	end

	if max == 0 then
		return str
	end

	repeat
		local s, e = str:find(pat, c, not regex)
		max = max - 1
		if s and max < 0 then
			t[#t+1] = str:sub(c)
		else
			t[#t+1] = str:sub(c, s and s - 1)
		end
		c = e and e + 1 or #str + 1
	until not s or max < 0

	return t
end

function Set(t)
  local s = {}
  for _,v in pairs(t) do s[v] = true end
  return s
end

function addToSet(set, key)
    set[key] = true
end

function removeFromSet(set, key)
    set[key] = nil
end

function setContains(set, key)
    return set[key] ~= nil
end


local log_dir = '/mnt/usb/clients'
local client_queries_log_files = scandir(log_dir)

for i,query_file in ipairs(client_queries_log_files) do
	
	local log_file_path = log_dir..'/'..query_file
	local log_file = io.open(log_file_path, "r")

	-- readable?

	if log_file == nil then 
		print('log file not readable. aborting.')
		print(log_file_path)
		return 
	end

	-- table of lines

	local log_file_str = log_file:read("*all")
	local log_table = split(log_file_str)

	local set_unique_domains = Set{}
	local table_unique_domains = {}

	for i,line in ipairs(log_table) do
  		local str_full_domain = split(line,' ')[3]
  		if str_full_domain ~= nil then
  			local domain_parts = split(str_full_domain,'.')
  		
  			-- use subdomains if possible

  			local domain = str_full_domain

  			if #domain_parts > 2 and domain_parts[#domain_parts-2] ~= 'www' then
				domain = domain_parts[#domain_parts-2]..'.'..domain_parts[#domain_parts-1]..'.'..domain_parts[#domain_parts]  			
			end
		
  			if not setContains(set_unique_domains,domain) then
  				addToSet(set_unique_domains,domain)
  				table.insert(table_unique_domains,domain)
  			end
  		end
	end

	log_file:close()

	-- write html file

	local file_name = string.sub(query_file,1,17)
	local html_file_name = file_name..'.html'
	local html_file_path = log_dir..'/'..html_file_name		
	local html_file = io.open(html_file_path, "w")

	for i,domain in ipairs(table_unique_domains) do
		local tr = '<tr id="'..domain..'"><td>'..i..'</td><td><a href="http://'..domain..'" target="_blank">'..domain..'</a></td><td><button class="btn btn-primary" value="0" onclick="domain_action(\''..domain..'\',this)">Sperren</button></td></tr>'
		html_file:write(tr..'\n')
	end

	html_file:close()

end
