#!/bin/sh

LOG_DIR='/mnt/usb/hotspot/logs'
CLIENTS_DIR='/mnt/usb/hotspot/clients'
TODAY=`date +%y-%m-%d`

if [ ! -e "$LOG_DIR" ]; then
  logger "clients.sh: $LOG_DIR does not exist. exiting."
  exit 1
fi

if [ ! -e "$CLIENTS_DIR" ]; then
  logger "clients.sh: $CLIENTS_DIR does not exist. exiting."
  exit 1
fi

# TEST
# TODAY='16-03-11'
# CLIENTS_DIR='/mnt/usb/clients-test'
# rm -r "$CLIENTS_DIR"
# mkdir -p "$CLIENTS_DIR"
# TEST END

DAILY_CLIENTS_LOG="$LOG_DIR/$TODAY-clients.log"
DAILY_QUERIES_LOG="$LOG_DIR/$TODAY-queries.log"
MAC_IP_TABLE="$LOG_DIR/$TODAY-mac-ip-table.csv"

CUT='/usr/bin/cut'
LOG='/usr/bin/logger'
AWK='/usr/bin/awk'
SED='/bin/sed'
GREP='/bin/grep'
ECHO='/bin/echo'
CAT='/bin/cat'
RM='/bin/rm'
MV='/bin/mv'
SORT='/usr/bin/sort'
WC='/usr/bin/wc'

START=$(date +%s)

# ADD ACCESS METADATA

while read LINE; do  

  CLIENT_MAC=`"$ECHO" "$LINE"|"$CUT" -d ' ' -f3`
  CLIENT_NAME=`"$ECHO" "$CLIENT_MAC"|"$SED" 's|:|-|g'`
  CLIENT_METAFILE="$CLIENT_NAME-meta.log"
  "$ECHO" "$TODAY $LINE" >> "$CLIENTS_DIR/$CLIENT_METAFILE"

done < "$DAILY_CLIENTS_LOG" 

# FIND USED IP ADDRESSES PER MAC ADDRESS AND BUILD A TABLE

while read LINE; do                                                                                

  OPERATOR=`"$ECHO" "$LINE"|"$CUT" -d ' ' -f2`
  MAC=`"$ECHO" "$LINE"|"$CUT" -d ' ' -f3`
  IP=`"$ECHO" "$LINE"|"$CUT" -d ' ' -f4`

  if [ "$OPERATOR" = "+" ]; then
    "$ECHO" "$MAC" "$IP" >> "$MAC_IP_TABLE"    
  fi                           

done < "$DAILY_CLIENTS_LOG" 

# REMOVE DUPLICATES FROM TABLE

"$SORT" -u < "$MAC_IP_TABLE" > "$MAC_IP_TABLE.uniq" && "$MV" "$MAC_IP_TABLE.uniq" "$MAC_IP_TABLE"

# LINK QUERIES TO MAC ADDRESSES

while read CLIENT; do

  CLIENT_NAME=`"$ECHO" "$CLIENT"|"$CUT" -d ' ' -f1|"$SED" 's/:/-/g'`
  CLIENT_IP=`"$ECHO" "$CLIENT"|"$CUT" -d ' ' -f2`

  # FORBID DUPLICATES - REMOVE ALL LINES FROM TODAY

  CQ="$CLIENTS_DIR/$CLIENT_NAME-queries.log"
  CQ2="$CLIENTS_DIR/$CLIENT_NAME-queries-til-today.log"
  
  "$GREP" -v "^$TODAY" < "$CQ" > "$CQ2" && "$MV" "$CQ2" "$CQ"

  while read QUERY; do

    QUERY_TIME=`"$ECHO" "$QUERY"|"$CUT" -d ' ' -f1`
    QUERY_IP=`"$ECHO" "$QUERY"|"$CUT" -d ' ' -f2`
    QUERY_DOMAIN=`"$ECHO" "$QUERY"|"$CUT" -d ' ' -f3`

    # ADD EVERY QUERY FROM TODAY TO CLIENT FILE
   
   if [ "$QUERY_IP" = "$CLIENT_IP" ]; then
    "$ECHO" "$TODAY" "$QUERY_TIME" "$QUERY_DOMAIN" >> "$CLIENTS_DIR/$CLIENT_NAME-queries.log"
   fi

  done < "$DAILY_QUERIES_LOG"

  # SORT QUERIES - MOST RECENT TO TOP

  "$SORT" -ur < "$CLIENTS_DIR/$CLIENT_NAME-queries.log" > "$CLIENTS_DIR/$CLIENT_NAME-queries-sorted.log" && "$MV" "$CLIENTS_DIR/$CLIENT_NAME-queries-sorted.log" "$CLIENTS_DIR/$CLIENT_NAME-queries.log"

done < "$MAC_IP_TABLE"

"$RM" "$MAC_IP_TABLE"



#
#
# GENERATE ADMIN INDEX HTML
#
#

/bin/sh /etc/hotspot/admin-status.sh


#
#
# GENERATE CLIENT HTML
#
#


/usr/bin/lua /etc/hotspot/client-domains.lua



END=$(date +%s)
DIFF=$(( $END - $START ))
# DIFF=$(( $DIFF / 60 ))

"$LOG" "Script $0 executed in $DIFF seconds."
"$ECHO" "Script $0 executed in $DIFF seconds."



exit 0

# Sat Mar 12 11:55:50 2016 user.notice root: Script client-logs.sh executed in 130 seconds.
