#!/bin/sh

usb_dir='/mnt/usb'
logs_dir="${usb_dir}/logs"
hotspot_dir="${usb_dir}/hotspot"
logs_dir="${hotspot_dir}"

if [ ! -e "$usb_dir" ]; 	then mkdir "$usb_dir"; fi
if [ ! -e "$hotspot_dir" ]; then mkdir "$hotspot_dir"; fi
# if [ ! -e "$logs_dir" ]; 	then mkdir "$logs_dir"; fi

/bin/cat "${logs_dir}/messages" | /bin/grep 'DHCPACK\|deauthenticated\|query\[A\]\|dropbear' | /bin/grep -v '127.0.0.1' >> "${logs_dir}/dhcp-dns.log"

/bin/dd if=/dev/null of="${logs_dir}/messages"