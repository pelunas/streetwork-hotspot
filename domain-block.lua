
function main()
	block_domain_action('abc.de')
	-- unblock_domain_action('abc.de')
end

function unblock_domain_action(domain)

	local user_line = domain
	local hosts_line = '0.0.0.0 ' .. domain

	local user_filename = './user-blocked-hosts.txt'
	local hosts_filename = './etc-hosts.txt'

	remove_line_from_file(user_line,user_filename)
	remove_line_from_file(hosts_line,hosts_filename)
end

function block_domain_action(domain)

	local user_line = domain
	local hosts_line = '0.0.0.0 ' .. domain

	local user_filename = './user-blocked-hosts.txt'
	local hosts_filename = './etc-hosts.txt'

	append_line_to_file( user_line , user_filename )
	append_line_to_file( hosts_line, hosts_filename )

end

function append_line_to_file(line,filename)
	local file = io.open(filename,'r+')	
	if file == nil then file = io.open(filename,'a+') end -- Append mode with read mode enabled that opens an existing file or creates a new file.
	local line_found = false
	for l in file:lines() do		
		if l == line then
			line_found = true
		end
	end
	if not line_found then
		file:write(line..'\n')
	end
	file:close()
end

function remove_line_from_file(line,filename)
	local idx = nil
	local ctr = 1	
	local file = io.open(filename,'r')
	for l in file:lines() do 
		if line == l then idx = ctr end
		ctr = ctr + 1
	end

	if idx ~= nil then
		remove_lines_from_file(filename,idx,1)
	end
end

function remove_lines_from_file( filename, starting_line, num_lines )
    
    local fp = io.open( filename, "r" )
    if fp == nil then return nil end
 
    content = {}
    i = 1;
    for line in fp:lines() do
        if i < starting_line or i >= starting_line + num_lines then
            content[#content+1] = line
        end
        i = i + 1
    end
 
    if i > starting_line and i < starting_line + num_lines then
        print( "Warning: Tried to remove lines after EOF." )
    end
 
    fp:close()
    fp = io.open( filename, "w+" )
 
    for i = 1, #content do
        fp:write( string.format( "%s\n", content[i] ) )
    end
 
    fp:close()
end

main()