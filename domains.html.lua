-- this script is part 2 of a series. part one is domains.sh.

-- part 1: domains.sh creates .txt and .html files
-- part 2: domains.html.lua creates  

local domains_dir = './../usb/domains'
local html_template = './../domains-table-html/index3.tpl.htm'
local tld_file_path = domains_dir..'/visited-top-level.txt'
local dd_file_path = domains_dir..'/visited-dd.txt'
local filename_schema = 'domains-c2-c1.html'
local split_at = 100

function log_table(t)
	print(tostring(t))
	for i,j in ipairs(t) do 
		print(j) 
	end
end

function ram_usage()
	print('memory usage : '..collectgarbage("count")*1024/1024)
end	

-- Lua implementation of PHP scandir function
-- plus grep for queries files

function scandir(directory)
	print('scanning dir' .. directory .. ' for \'queries\' files...')
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls "'..directory..'"'..'|grep queries')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

function scandir2(directory)
	print('scanning dir' .. directory .. ' for \'visited\' files...')
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls -t "'..directory..'"'..'|grep visited')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

function split(str, pat, max, regex)	
	pat = pat or "\n"
	max = max or #str

	local t = {}
	local c = 1

	if #str == 0 then
		return {""}
	end

	if #pat == 0 then
		return nil
	end

	if max == 0 then
		return str
	end

	repeat
		local s, e = str:find(pat, c, not regex)
		max = max - 1
		if s and max < 0 then
			t[#t+1] = str:sub(c)
		else
			t[#t+1] = str:sub(c, s and s - 1)
		end
		c = e and e + 1 or #str + 1
	until not s or max < 0

	return t
end

function Set(t)
  local s = {}
  for _,v in pairs(t) do s[v] = true end
  return s
end

function addToSet(set, key)
    set[key] = true
end

function removeFromSet(set, key)
    set[key] = nil
end

function setContains(set, key)
    return set[key] ~= nil
end

function string_from_file( file_path )
	local file = io.open(file_path, "r")
	-- readable?
	if file == nil then return nil end
	local str = file:read("*all")
	file.close()
	ram_usage()
	return str
end

function table_from_file( file_path )
	print('creating table from file : ' .. file_path )
	local file = io.open(file_path, "r")
	if file == nil then return nil end
	local str = file:read("*all")
	file.close()
	local tab = split(str)
	ram_usage()
	return tab	
end

function file_from_table( table, file_path )
	print('create file from table : ' .. file_path)
	local file = io.open(file_path, "w")
	for i,e in ipairs(table) do	
		file:write(e..'\n')
	end
	file:close()
	ram_usage()
end

function file_from_string( string, file_path )
	local file = io.open(file_path, "w")
	file:write(string)
	file:close()
end

-- creates file of unique top level domains
-- list of input files

print('search for files')
ram_usage()


local source_files = scandir2(domains_dir)
log_table(source_files)

os.remove(tld_file_path) -- clear
os.remove(dd_file_path) 


local set_tld = Set{}
local table_tld = {}

print('fill table with unique top level domain names...')
ram_usage()
for i,sf in ipairs(source_files) do 

	local file_path = domains_dir..'/'..sf
	-- print(file_path)
	local domains_tab = table_from_file(file_path)	
	if domains_tab ~= nil then

		for j,domain in ipairs(domains_tab) do 
			
			local domain_parts = split(domain,'.')
			
			if #domain_parts > 1 then
			
				local tld_str = domain_parts[#domain_parts-1]..'.'..domain_parts[#domain_parts]
				
				if not setContains(set_tld,tld_str) then
					addToSet(set_tld,tld_str)
					table.insert(table_tld,tld_str)
				end
			end
		end
	end
end


ram_usage()

-- os.exit(0)
print('sort table')

table.sort(table_tld)
file_from_table(table_tld,tld_file_path)


-- loop over top level, then loop over source files and find matches

-- html template
print('load html template')
local index_tpl = string_from_file(html_template)


local tld_tpl = [===[

<li class="top-domain" onmouseover="show_subs(this)" onmouseout="hide_subs(this)" >
	<table><tr><td class="lc"><a href="http://{{domain}}" target="_blank">{{domain}}</a></td><td><button value="0" onclick="tog(this)">sperren</button></td></tr></table>
	<ul>
		{{sld}}		
	</ul>
</li>

]===]

local sld_tpl = [===[
		
		<li class="sub-domain"><table><tr><td class="lc"><a href="http://{{domain}}" target="_blank">{{domain}}</a></td><td><button value="0" onclick="tog(this)">sperren</button></td></tr></table></li>

]===]


local pager_tpl = '<li><a href="{{link}}">{{count}}</a></li>'..'\n'

-- html template end

print('loop over table with every top level domain name')

local c1 = 0
local c2 = 1

local filename_counter = split_at
local filename = string.gsub(filename_schema,'c1',filename_counter)
filename = string.gsub(filename,'c2',c2)
local page_file_path = domains_dir..'/'..filename
os.remove(page_file_path) -- clear

local list_of_html_files = {}


local html_str = ''

for i,tld in ipairs(table_tld) do

	local tld_html_str = string.gsub(tld_tpl,'{{domain}}',tld)
	-- local tld_html_str = string.gsub(tld_html_str,'{{count}}',c1)

	if c1 > filename_counter then

		local page_file_path = domains_dir..'/'..filename
		table.insert(list_of_html_files,filename)

		os.remove(page_file_path) -- clear
		local ac = string.gsub(index_tpl,'{{tld}}',html_str)
		file_from_string(ac,page_file_path)
		html_str = ''

		filename_counter = filename_counter + split_at
		c2 = c2 + 1

		filename = string.gsub(filename_schema,'c1',filename_counter)
		filename = string.gsub(filename,'c2',c2)

	end

	c1 = c1 + 1

	local list_of_sld_str = ''
	
	for i,sf in ipairs(source_files) do 

		local file_path = domains_dir..'/'..sf
		local domains_tab = table_from_file(file_path)
		
		if domains_tab ~= nil then

			for j,domain in ipairs(domains_tab) do 
				
				local domain_parts = split(domain,'.')
				
				if #domain_parts > 1 then
				
					local tld_str = domain_parts[#domain_parts-1] .. '.' .. domain_parts[#domain_parts]
					
					if tld == tld_str then
						if domain == tld or domain == 'www.' .. tld then -- ignore top level domains
							-- table.insert(table_dd,' >> '..domain)
						else
							-- table.insert(table_dd,' > '..domain)
							local aa = string.gsub(sld_tpl,'{{domain}}',domain)
							list_of_sld_str = list_of_sld_str .. aa
						end
					end
				end
			end
		end		
	end

	html_str = html_str .. string.gsub(tld_html_str,'{{sld}}',list_of_sld_str)
end

-- last file (last counter dont reach the split point)

filename = string.gsub(filename_schema,'c1',filename_counter)
filename = string.gsub(filename,'c2',c2)

local page_file_path = domains_dir..'/'..filename
os.remove(page_file_path) -- clear
local ac = string.gsub(index_tpl,'{{tld}}',html_str)
file_from_string(ac,page_file_path)
table.insert(list_of_html_files,filename)

html_str = ''

ram_usage()
print('loop over all html pages and insert pager')

print('create pager html')

local pager_html_total = ''
local pager_html = ''

for i,e in ipairs(list_of_html_files) do 

	local pager_link = e -- 'path/to/' .. e
	local pager_counter = i

	pager_html = string.gsub(pager_tpl,'{{link}}',pager_link)
	pager_html = string.gsub(pager_html,'{{count}}',pager_counter)

	pager_html_total = pager_html_total .. pager_html

end
ram_usage()
print('insert pager html')

for i,e in ipairs(list_of_html_files) do

	local file_path = domains_dir .. '/' .. e
	local page_html_str = string_from_file(file_path)
	local page_html_str_pp = string.gsub(page_html_str,'{{pager}}',pager_html_total)
	file_from_string(page_html_str_pp,file_path)

end








ram_usage()
os.exit(0)





































local tld_file = io.open("usb/clients/60-d9-c7-2f-25-7a-top-level-domains.log", "r")
local tld_str = tld_file:read("*all")
local tld_tab = split(tld_str)

local fqdn_file = io.open("usb/clients/60-d9-c7-2f-25-7a-fqdn-domains.log", "r")

local fqdn_str = fqdn_file:read("*all")
local fqdn_tab = split(fqdn_str)

local html_file = io.open("usb/clients/60-d9-c7-2f-25-7a-ul-table.html", "w")
local html_str = ''
local sld_str = ''


for i,tld_line in ipairs(tld_tab) do

	local tld_partial = string.gsub(tld_tpl,'{{domain}}',tld_line)
	local sld_str = ''

	for j,fqdn_line in ipairs(fqdn_tab) do 
		
		local fqdn_parts = split(fqdn_line,'.')
		
		if #fqdn_parts > 1 then

			local fqdn_tld = fqdn_parts[#fqdn_parts-1]..'.'..fqdn_parts[#fqdn_parts]
			
			if tld_line == fqdn_tld then

				local www_fqdn_tld = 'www.'..fqdn_tld -- exclude www subdomains

				if www_fqdn_tld ~= fqdn_line then
				
					local sld_partial = string.gsub(sld_tpl,'{{domain}}',fqdn_line)
					sld_str = sld_str..sld_partial

				end
			end	
		end
	end

	html_partial = string.gsub(tld_partial,'{{sld}}',sld_str)
	html_str = html_str..html_partial

end

index_html_str = string.gsub(index_tpl,'{{tld}}',html_str)

html_file:write(index_html_str)

tld_file.close()
fqdn_file.close()
html_file.close()

os.exit(0)
























local client_queries_log_files = scandir(log_dir)

for i,query_file in ipairs(client_queries_log_files) do
	
	local log_file_path = log_dir..'/'..query_file
	local log_file = io.open(log_file_path, "r")

	-- readable?

	if log_file == nil then 
		print('log file not readable. aborting.')
		print(log_file_path)
		return 
	end

	-- table of lines

	local log_file_str = log_file:read("*all")
	local log_table = split(log_file_str)

	local set_unique_domains = Set{}
	local table_unique_domains = {}

	for i,line in ipairs(log_table) do

  		local str_full_domain = split(line,' ')[3]
  		
  		if str_full_domain ~= nil then

  			local domain_parts = split(str_full_domain,'.')
  		
  			-- use subdomains if possible

  			local domain = str_full_domain

  			if #domain_parts > 2 and domain_parts[#domain_parts-2] ~= 'www' then
				domain = domain_parts[#domain_parts-2]..'.'..domain_parts[#domain_parts-1]..'.'..domain_parts[#domain_parts]  			
			end
		
  			if not setContains(set_unique_domains,domain) then
  				addToSet(set_unique_domains,domain)
  				table.insert(table_unique_domains,domain)
  			end
  		end
	end

	log_file:close()

	-- write html file

	local file_name = string.sub(query_file,1,17)
	local html_file_name = file_name..'.html'
	local html_file_path = log_dir..'/'..html_file_name		
	local html_file = io.open(html_file_path, "w")

	for i,domain in ipairs(table_unique_domains) do
		local tr = '<tr id="'..domain..'"><td>'..i..'</td><td><a href="http://'..domain..'" target="_blank">'..domain..'</a></td><td><button class="btn btn-primary" value="0" onclick="domain_action(\''..domain..'\',this)">Sperren</button></td></tr>'
		html_file:write(tr..'\n')
	end

	html_file:close()

end
