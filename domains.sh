#!/bin/bash

# creates a list of domains split to multiple files

domains_dir='./../usb/domains'
split_at=100
counter=0
counter2=1
filename_counter=$split_at

file_schema='visited-c2-cc.txt'

queries_file_0="${domains_dir}/visited-0.txt"
queries_file_1="${domains_dir}/visited-1.txt"

filename=$(echo $file_schema | sed -e "s/cc/${filename_counter}/g" -e "s/c2/${counter2}/g")

# delete everythin in putput folder

find "$domains_dir" -type f -name 'visited-*.txt' -delete

# daily queries into one file

for i in $(ls ./../usb/logs/*/queries.log); do cat "$i"|cut -d ' ' -f3 >> "$queries_file_0"; done

cat "$queries_file_0"|sort -u > "$queries_file_1"

# remove duplicates

for j in $(cat $queries_file_1); do
	

	if [ "$counter" -gt "$filename_counter" ]; then
		
		filename_counter=$(( $filename_counter + $split_at ))
		counter2=$(( $counter2 + 1 ))

		filename=$(echo $file_schema | sed -e "s/cc/${filename_counter}/g" -e "s/c2/${counter2}/g")

	fi

	echo "$j" >> "${domains_dir}/${filename}"
		
	counter=$(( $counter + 1 ))

done

rm "$queries_file_0" "$queries_file_1"
