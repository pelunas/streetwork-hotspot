
function main()

	-- if arg[1] == nil and arg[2] == nil then print('usage: convert.lua file.txt file.conf') return end

	print(arg[1],arg[2])

	local file_in = arg[1]
	local file_out = arg[2]
	
	hosts_to_dnsmasq(file_in,file_out)
end


function hosts_to_dnsmasq(file_in,file_out)
	local file = io.open(file_in,'r')
	if file == nil then print('file not found') return end
	local lines = {}
	for line in file:lines() do
		
		if string.sub(line,1,1) == '#' then -- keep comments
			table.insert(lines,line)
		elseif line ~= "" then
			local l = split(line,' ')
			local d = l[2]
			if d ~= nil then
				table.insert(lines,d)
			end
		end
	end
	file:close()
	file = io.open(file_out,'w')
	for i,v in ipairs(lines) do
		if string.sub(v,1,1) == '#' then -- keep comments
			file:write(v..'\n')	
		else
			file:write(string.format('address=/%s/0.0.0.0\n',v))
		end
	end
	file:close()
end


function split(str, pat, max, regex)    
    pat = pat or "\n"
    max = max or #str

    local t = {}
    local c = 1

    if #str == 0 then
        return {""}
    end

    if #pat == 0 then
        return nil
    end

    if max == 0 then
        return str
    end

    repeat
        local s, e = str:find(pat, c, not regex)
        max = max - 1
        if s and max < 0 then
            t[#t+1] = str:sub(c)
        else
            t[#t+1] = str:sub(c, s and s - 1)
        end
        c = e and e + 1 or #str + 1
    until not s or max < 0

    return t
end


main()