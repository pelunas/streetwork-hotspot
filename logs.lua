
local process_logs          = true
local process_clients       = true
local process_domains       = true

local client_logs_split_at  = 50
local should_clear          = true

-- local test paths

local logs_dir              = './logs'
local daily_logs_dir        = './logs/daily'
local clients_dir           = './clients'
local domains_dir           = './domains'
local domains_html_dir      = './domains/html'
local logs_file             = './dhcp-dns.log'
local dhcp_db_file          = './dhcp.db.txt'
local dhcp_leases_file      = './dhcp.leases.txt'

local process_lines_starting_at = 1
local process_line_count        = 250

local dhcp_db = nil
local client_tables = {}
local domain_tables = {}
local logs_table = {}
local unique_domains_set = nil
local running_on_device = false

local start_time = os.clock()

function main()

    running_on_device = (exec_line('uname') == 'Linux')
    
    if running_on_device then        
        logs_dir              = '/mnt/usb/hotspot/logs'
        daily_logs_dir        = '/mnt/usb/hotspot/logs/daily'
        clients_dir           = '/mnt/usb/hotspot/clients'
        domains_dir           = '/mnt/usb/hotspot/domains'
        domains_html_dir      = '/mnt/usb/hotspot/domains/html'
        logs_file             = '/mnt/usb/hotspot/logs/dhcp-dns.log'
        dhcp_db_file          = '/mnt/usb/hotspot/dhcp-db.txt'
        dhcp_leases_file      = '/tmp/dhcp.leases'
    end

    print('process_logs',process_logs)
    print('process_clients',process_clients)
    print('process_domains',process_domains)
    print('client_logs_split_at',client_logs_split_at)
    print('should_clear',should_clear)    

    print('process_lines_starting_at',process_lines_starting_at)
    print('process_line_count',process_line_count)

    print('logs_dir',logs_dir)
    print('clients_dir',clients_dir)
    print('domains_dir',domains_dir)
    print('domains_html_dir',domains_html_dir)
    print('logs_file',logs_file)
    print('dhcp_db_file',dhcp_db_file)
    print('dhcp_leases_file',dhcp_leases_file)

    unique_domains_set = Set{}
    
    -- ram_usage()

    -- todo: load public suffix list
    
    local logs = lines_from_file(logs_file,process_lines_starting_at,process_line_count)
    
    if not logs then
        print('could not found logs file at: '..logs_file)
        os.exit(1)
    end

    dhcp_db = update_dhcp_db()

    if process_clients then client_tables = create_client_tables() end
    
    create_folders()

    -- if dhcp_leases == nil then
    --     print('could not read from /tmp/dhcp.leases')
    -- end


    -- read and process logs

    for i,v in ipairs(logs) do
        
        local a = string.find(v,"query[A]",1,true)
        local b = string.find(v,'DHCPACK',1,true)
        local c = string.find(v,'deauthenticated',1,true)
        -- local d = string.find(v,'dropbear',1,true)

        local p = split(v,' ')
        local ip = nil
        local mac = nil
        local line = nil
        local full_domain = nil

        local month = p[2]
        local day = p[3]
        local year = p[5]

        if a ~= nil then
            
            ip = p[13]
            mac = client_name_for_ip(ip)
            full_domain = p[11]
            line = p[1]..' '..p[2]..' '..p[3]..' '..p[4]..' '..p[5]..' '..full_domain

            if process_clients then append_client_table(mac,line) end
            if process_domains then append_domain_table(full_domain) end
        
        elseif b ~= nil then

            ip = p[9]
            mac = p[10]
            host = p[11]
            
            macn = string.gsub(mac,':','-')
            line = p[1]..' '..p[2]..' '..p[3]..' '..p[4]..' '..p[5].. ' +++ authenticated ' .. ip .. ' ' .. host 

            if process_clients then 
                add_dhcp_db(mac,ip,host)
                append_client_table(macn,line) 
            end
        
        elseif c ~= nil then

           mac = p[10]
           macn = string.gsub(mac,':','-')
           line = p[1]..' '..p[2]..' '..p[3]..' '..p[4]..' '..p[5]..' --- deauthenticated '

           if process_clients then append_client_table(macn,line) end

        end

        if process_logs then append_logs_table(day,month,year,v) end

    end

    if process_clients then save_client_logs() end
    if process_domains then save_domain_logs() end
    if process_logs then save_logs_table() end
    
    if should_clear then 
        remove_lines_from_file(logs_file,process_lines_starting_at,process_line_count)
    end

    local duration = os.clock() - start_time
    print('script executed in ' .. duration .. ' seconds')
    syslog('lua.logs script executed in ' .. duration .. ' seconds')
    
    
    -- loop over domain tables
    -- for k,v in pairs(domain_tables) do
    --     print(k)        
    --     for a,b in pairs(v) do
    --         print(a,b)
    --     end 
    -- end

    -- loop over client tables
    -- for k,v in pairs(client_tables) do
    --     print('lines for mac',k)        
    --     for a,b in pairs(v) do
    --         print(a,b)
    --     end 
    -- end


    -- 1) clients - make a list of 

    -- for i,j in ipairs(dhcp_db) do
        
    --     local c_mac   = j[1]
    --     local c_ip    = j[2]
    --     local c_host  = j[3]

    --     print(c_mac,c_ip,c_host)



    -- end

    


    -- 2) domain names

    

    -- 3) raw dhcp and dns query logs by day


    -- 4) clear logs file

    -- remove_lines_from_file(logs_file,1,50)

    
end

function syslog( str )
    os.execute('/usr/bin/logger ' .. str)
end


function append_client_table(mac,line)
    if mac ~= nil then
        local t = client_tables[mac]
        if t == nil then
            client_tables[mac] = {}
            t = client_tables[mac]
        end
        table.insert(t,line)
    end
end

function append_logs_table(day,month,year,line)
    
    local filename = year .. '-' .. month .. '-' .. day

    if logs_table[filename] == nil then logs_table[filename] = {} end 
    
    table.insert(logs_table[filename],line)
end



function append_domain_table(full_domain)
    
    local p = split(full_domain,'.')
    
    if #p > 1 then
    
        local tld = p[#p-1]..'.'..p[#p] -- todo: check if public suffix
            
        local t = domain_tables[tld]

        if t == nil then
            domain_tables[tld] = {}
            t = domain_tables[tld]
        end        

        if tld ~= full_domain and full_domain ~= 'www.'..tld then
            if not setContains(unique_domains_set,full_domain) then
                addToSet(unique_domains_set,full_domain)
                table.insert(t,full_domain)
            end            
        end
    end
end

-- #save

function save_client_logs()
    
    for k,v in pairs(client_tables) do
        
        
        local wd = clients_dir.. '/' .. k
        if not file_exists(wd) then os.execute('mkdir '..wd) end
        local files = scandir(wd)
        
        -- use incremental filenames

        local ctr = 10000000
        local filename = tostring(ctr)..'.txt'
        
        local wfile = nil
        local lc_wf = 0
        local wf = wd .. '/' .. filename

        if #files > 0 then
            ctr = tonumber(string.sub(files[1],1,8))
            wf = wd .. '/' .. files[1]        
            lc_wf = linecount_in_file(wf)
        end

        -- print(k,wf,lc_wf)

        -- save contents of tables to files

        wfile = io.open(wf, "a")

        for a,b in pairs(v) do
            
            if lc_wf >= client_logs_split_at then -- split point reached, use new file
                
                wfile:close()
                ctr = ctr + 1
                filename = ''..tostring(ctr)..'.txt'
                wf = wd .. '/' .. filename
                wfile = io.open(wf, "a")  
                -- print(wf)
                lc_wf = 0

            end

            wfile:write(b..'\n')
            lc_wf = lc_wf + 1
            
        end 

        wfile:close()
    end
end

function save_domain_logs()    

    for k,v in pairs(domain_tables) do

        if k ~= nil then

            local wf = domains_dir .. '/' .. k
            
            local fd = table_from_file(wf)

            if fd == nil then fd = {} end

            -- for i,v in ipairs(fd) do
            --     print(i,v)
            -- end
            
            local wfile = io.open(wf,"a")
            -- print(k)        
            for a,b in pairs(v) do
                
                local found = false

                for m,n in ipairs(fd) do

                    if n == b then
                        found = true
                    end
                end

                if not found then
                   wfile:write(b..'\n')
                end

                
            end 

            wfile:close()
        end
    end
end

function save_logs_table()
    for k,v in pairs(logs_table) do
        local wf = daily_logs_dir .. '/' .. k .. '.log'
        local wfile = io.open(wf, "a")        
        for i,j in ipairs(v) do            
            wfile:write(j..'\n')
        end
        wfile:close()
    end
end

function linecount_in_file(filename)
    local ctr = 0
    for _ in io.lines(filename) do
        ctr = ctr + 1
    end
    return ctr
end

function create_client_tables()
    local t = {}
    for i,v in ipairs(dhcp_db) do
        local db_mac = v[1]
        local n = string.gsub(db_mac,':','-')       
        t[n] = {}
    end

    return t
end

function create_folders()
    -- /mnt/usb/hotspot/logs/
    if not file_exists(logs_dir) then os.execute('mkdir '..logs_dir) end
    -- /mnt/usb/hotspot/logs/daily/
    if not file_exists(daily_logs_dir) then os.execute('mkdir '..daily_logs_dir) end
    -- /mnt/usb/hotspot/domains/html/
    -- if not file_exists(domains_html_dir) then os.execute('mkdir '..domains_html_dir) end
    -- /mnt/usb/hotspot/domains/
    if not file_exists(domains_dir) then os.execute('mkdir '..domains_dir) end
    -- /mnt/usb/hotspot/clients/
    if not file_exists(clients_dir) then os.execute('mkdir '..clients_dir) end
    -- /mnt/usb/hotspot/clients/34-11-e4-35-32-6a
    for i,v in ipairs(dhcp_db) do
        local db_mac = v[1]
        local n = string.gsub(db_mac,':','-')        
        if not file_exists(clients_dir..'/'..n) then
            os.execute('mkdir '..clients_dir..'/'..n)        
        end
    end
end

function client_name_for_ip(ip)
    for i,v in ipairs(dhcp_db) do
        local db_mac = v[1]
        local db_ip = v[2]
        if ip == db_ip then return string.gsub(db_mac,':','-') end
    end
    return nil 
end


-- #dhcp

function add_dhcp_db(mac,ip,host)
    local db_file  = dhcp_db_file
    local found = false
    for i,v in ipairs(dhcp_db) do
        local db_mac = v[1]
        local db_ip = v[2]
        local db_host = v[3]
        if mac == db_mac then
            v[2] = ip
            v[3] = host
            found = true
        end        
    end
    if not found then
        table.insert(dhcp_db, {mac,ip,host} )
    end
    local file = io.open(db_file, "w")
    local string = ''
    for i,v in ipairs(dhcp_db) do            
        string = string .. v[1] .. ' ' .. v[2] .. ' ' .. v[3] .. '\n'
    end
    file:write(string)
    file:close()
end

function update_dhcp_db()

    sys_file = dhcp_leases_file -- 'dhcp.leases.txt'       -- /tmp/dhcp.leases
    db_file  = dhcp_db_file     -- 'dhcp.db.txt'    /etc/hotspot/dhcp.db.txt

    local leases_now    = table_from_file(sys_file)
    local leases_db     = table_from_file(db_file)
    local leases_db2    = nil
    local leases_now2   = nil

    if leases_now ~= nil then
        leases_now2 = {}         
        for i,j in ipairs(leases_now) do            
            local p = split(j,' ')
            local mac = p[2]
            local ip = p[3]
            local host = p[4]            
            if mac ~= nil and ip ~= nil then
                table.insert(leases_now2,{mac,ip,host})
            end
        end
    end

    if leases_db ~= nil then
        leases_db2 = {} 
        for i,j in ipairs(leases_db) do            
            local p = split(j,' ')
            local mac = p[1]
            local ip = p[2]
            local host = p[3]
            if mac ~= nil and ip ~= nil then
                table.insert(leases_db2,{mac,ip,host})
            end
        end
    end

    if leases_db2 == nil and leases_now2 == nil then -- no data at all
        
        return nil

    elseif leases_db2 == nil and leases_now2 ~= nil then -- write new db file

        local file = io.open(db_file, "w")
        local string = ''
        for i,v in ipairs(leases_now2) do            
            string = string .. v[1] .. ' ' .. v[2] .. ' ' .. v[3] .. '\n'
        end
        file:write(string)
        file:close()

        return leases_now2

    elseif leases_db2 ~= nil and leases_now2 == nil then

        return leases_db2

    elseif leases_db2 ~= nil and leases_now2 ~= nil then -- update db

        for i,j in ipairs(leases_now2) do

            local now_mac  = j[1]
            local now_ip   = j[2]
            local now_host = j[3]
            
            local mac_found = false
            for k,l in ipairs(leases_db2) do

                local db_mac = l[1]
                local db_ip  = l[2] 
                local db_host = l[3]

                if now_mac == db_mac then
                    l[2] = now_ip
                    l[3] = now_host
                    mac_found = true
                end

                -- print(k,l)
            end

            if mac_found == false then                
                table.insert(leases_db2,{now_mac,now_ip,now_host})                
            end

            -- print(i,j)
        end

        local file = io.open(db_file, "w")
        local string = ''
        for i,v in ipairs(leases_db2) do
            -- print(i,v[1],v[2],v[3])       
            string = string .. v[1] .. ' ' .. v[2] .. ' ' .. v[3] .. '\n'
        end
        file:write(string)
        file:close()

    end
    
    return leases_db2
end

function lines_from_file( filename, starting_line, num_lines )
    -- print('lines from file : ' .. filename )
    local fp = io.open( filename, "r" )
    if fp == nil then return nil end
    
    content = {}
    i = 1;
    for line in fp:lines() do
        if i >= starting_line and i < starting_line + num_lines then
            content[#content+1] = line
            -- print(line)
        end        
        i = i + 1
    end
    
    if i > starting_line and i < starting_line + num_lines then
        print( "Warning: Tried to remove lines after EOF." )
    end
    
    fp:close()


    -- for i,j in ipairs(content) do
    --     print(tostring(i) .. ' : ' .. content[i])
    -- end

    -- ram_usage()
    return content  
end

function table_from_file( file_path )
    -- print('create table from file : ' .. file_path )
    local file = io.open(file_path, "r")
    if file == nil then return nil end
    local str = file:read("*all")
    file:close()
    local tab = split(str)
    -- ram_usage()
    return tab  
end




function remove_lines_from_file( filename, starting_line, num_lines )
    
    local fp = io.open( filename, "r" )
    if fp == nil then return nil end
 
    content = {}
    i = 1;
    for line in fp:lines() do
        if i < starting_line or i >= starting_line + num_lines then
            content[#content+1] = line
        end
        i = i + 1
    end
 
    if i > starting_line and i < starting_line + num_lines then
        print( "Warning: Tried to remove lines after EOF." )
    end
 
    fp:close()
    fp = io.open( filename, "w+" )
 
    for i = 1, #content do
        fp:write( string.format( "%s\n", content[i] ) )
    end
 
    fp:close()
end

function split(str, pat, max, regex)    
    pat = pat or "\n"
    max = max or #str

    local t = {}
    local c = 1

    if #str == 0 then
        return {""}
    end

    if #pat == 0 then
        return nil
    end

    if max == 0 then
        return str
    end

    repeat
        local s, e = str:find(pat, c, not regex)
        max = max - 1
        if s and max < 0 then
            t[#t+1] = str:sub(c)
        else
            t[#t+1] = str:sub(c, s and s - 1)
        end
        c = e and e + 1 or #str + 1
    until not s or max < 0

    return t
end


function ram_usage()
    print('memory usage : '..collectgarbage("count")*1024/1024)
end 

function log_table(t)
    print(tostring(t))
    for i,j in ipairs(t) do 
        print(j) 
    end
end

function file_exists(name)
   local f = io.open(name,"r")
   if f ~= nil then io.close(f) return true else return false end
end

function scandir(directory)
    -- print('scan dir ' .. directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls -r "'..directory..'"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

function Set(t)
  local s = {}
  for _,v in pairs(t) do s[v] = true end
  return s
end

function addToSet(set, key)
    set[key] = true
end

function removeFromSet(set, key)
    set[key] = nil
end

function setContains(set, key)
    return set[key] ~= nil
end

function exec_line(command)
    local pp   = io.popen(command)
    local data = pp:read("*line")
    pp:close()

    return data
end

function exec(command)
    local pp   = io.popen(command)
    local data = pp:read("*a")
    pp:close()

    return data
end

main()