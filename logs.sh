#!/bin/sh

usb_dir='/mnt/usb'
hotspot_dir="${usb_dir}/hotspot"
logs_dir="${hotspot_dir}/logs"
filtered_logs="${logs_dir}/dhcp-dns.log"
grep_str='DHCPACK\|deauthenticated\|query\[A\]\|authpriv\.notice\|user\.notice'

if [ ! -e "$usb_dir" ]; 	then mkdir "$usb_dir"; fi
if [ ! -e "$hotspot_dir" ]; then mkdir "$hotspot_dir"; fi
if [ ! -e "$logs_dir" ];    then mkdir "$logs_dir" ; fi

# grep syslog 

/bin/cat "${logs_dir}/messages" | /bin/grep "$grep_str" | /bin/grep -v '127.0.0.1' >> "$filtered_logs"

# clear syslog

/bin/dd if=/dev/null of="${logs_dir}/messages"

# clients / domains / daily logs

/usr/bin/lua /etc/hotspot/logs.lua 

# create html 



exit 0

