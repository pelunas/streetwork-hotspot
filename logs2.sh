#!/bin/sh

# 
# 
# store every client access
# store clients leaving network
# store dns queries
# empty syslog
#

LOG_DIR='/mnt/usb/logs'
LOG_FILE="$LOG_DIR/messages"

TODAY=`date +%y-%m-%d`

DAILY_LOG_DIR="${LOG_DIR}/${TODAY}"

DAILY_CLIENTS="${DAILY_LOG_DIR}/clients.log"
DAILY_QUERIES="${DAILY_LOG_DIR}/queries.log"

GREP='/bin/grep'
DD='/bin/dd'
AWK='/usr/bin/awk'
SORT='/usr/bin/sort'
MV='/bin/mv'
MKDIR='/bin/mkdir'
LOGGER='/usr/bin/logger'

# CHECK IF LOG DIR EXIST.

if [ ! -e "$DAILY_LOG_DIR" ]; then	
	"$MKDIR" "$DAILY_LOG_DIR"
fi

# CHECK IF SYSLOG FILE EXIST.

if [ ! -e "$LOG_FILE" ]; then	
	"$LOGGER" "error in logs.sh. $LOG_FILE does not exist."
	
	/etc/init.d/log disable
	/etc/init.d/log enable
	/etc/init.d/log restart
	exit 0
fi

# CLIENT ENTERS NETWORK >>> TIME + MAC IP HOSTNAME

"$GREP" 'DHCPACK' < "$LOG_FILE"|"$AWK" '{print $4,"+",$10,$9,$11}' >> "$DAILY_CLIENTS"

# CLIENT LEAVES NETWORK >>> TIME - MAC

"$GREP" 'deauthenticated' < "$LOG_FILE"|"$AWK" '{print $4,"-",$10}' >> "$DAILY_CLIENTS"

# SORT BY TIME

"$SORT" "$DAILY_CLIENTS" > "$DAILY_LOG_DIR/cs.tmp" && "$MV" "$DAILY_LOG_DIR/cs.tmp" "$DAILY_CLIENTS"

# CLIENT QUERIES >>> TIME IP DOMAIN

"$GREP" 'query\[A\]' < "$LOG_FILE"|"$GREP" -v "127.0.0.1"|"$AWK" '{print $4,$13,$11}' >> "$DAILY_QUERIES"

# CLEAR SYSLOG FILE

"$DD" if=/dev/null of="$LOG_FILE"

exit 0

