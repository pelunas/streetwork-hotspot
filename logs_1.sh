#!/bin/sh

# 
# 
# store every client access
# store clients leaving network
# store dns queries
# empty syslog
#

LOG_DIR='/mnt/usb/logs'
LOG_FILE="$LOG_DIR/messages"
TODAY=`date +%y-%m-%d`

DAILY_CLIENTS="$LOG_DIR/$TODAY-clients.log"
DAILY_CLIENTS_SORTED="$LOG_DIR/$TODAY-clients-sorted.log"
DAILY_QUERIES_LOG="$LOG_DIR/$TODAY-queries.log"

GREP='/bin/grep'
DD='/bin/dd'
AWK='/usr/bin/awk'
SORT='/usr/bin/sort'
MV='/bin/mv'
MKDIR='/bin/mkdir'
LOGGER='/usr/bin/logger'

# CHECK IF SYSLOG FILE EXIST.

if [ ! -e "$LOG_FILE" ]; then
	"$LOGGER" "error in logs.sh. $LOG_FILE does not exist."
	# sed 's/START=12/START=42/' < '/etc/init.d/log' > '/tmp/log.init.sh' && mv '/tmp/log.init.sh' '/etc/init.d/log' && chmod +x '/etc/init.d/log'
	/etc/init.d/log disable
	/etc/init.d/log enable
	/etc/init.d/log restart
	exit 0
fi

# CLIENT ENTERS NETWORK >>> TIME + MAC IP HOSTNAME

"$GREP" 'DHCPACK' < "$LOG_FILE"|"$AWK" '{print $4,"+",$10,$9,$11}' >> "$DAILY_CLIENTS"

# CLIENT LEAVES NETWORK >>> TIME - MAC

"$GREP" 'deauthenticated' < "$LOG_FILE"|"$AWK" '{print $4,"-",$10}' >> "$DAILY_CLIENTS"

# SORT BY TIME

"$SORT" "$DAILY_CLIENTS" > "$DAILY_CLIENTS_SORTED" && "$MV" "$DAILY_CLIENTS_SORTED" "$DAILY_CLIENTS"

# CLIENT QUERIES >>> TIME IP DOMAIN

"$GREP" 'query\[A\]' < "$LOG_FILE"|"$AWK" '{print $4,$13,$11}' >> "$DAILY_QUERIES_LOG"

# CLEAR SYSLOG FILE

"$DD" if=/dev/null of="$LOG_FILE"

exit 0

