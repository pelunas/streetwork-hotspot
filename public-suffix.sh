#!/bin/bash

# filter unusual domains

cat public_suffix_list_clean.txt | grep -v "museum\|aero\|.jp" > public_suffix_list_clean-#1.txt

# only entries containing a point

cat public_suffix_list_clean-#1.txt | grep "\\." > public_suffix_list_clean-#2.txt