#!/bin/sh

# launch locally with 
# sh setup.sh


echo "USB MOUNTPOINT /mnt/usb ..."

mkdir -p /mnt/usb

echo "UCI CONFIGS..."

cp /etc/hotspot/etc/config/* /etc/config/

echo "BOOT: USB AUTOREPAIR..."

cp /etc/hotspot/etc/init.d/fsck /etc/init.d && chmod +x /etc/init.d/fsck
/etc/init.d/fsck enable

echo "BOOT: LATE NETWORK RESTART (NEEDS FIX)..."

cp /etc/hotspot/etc/init.d/network-restart /etc/init.d && chmod +x /etc/init.d/network-restart
/etc/init.d/network-restart enable

echo "BOOT: CREATE HOTSPOT FOLDER ON USB STICK..."

cp /etc/hotspot/etc/init.d/mkir /etc/init.d && chmod +x /etc/init.d/mkdir
/etc/init.d/mkdir enable

echo "BOOT: START LOGGING AFTER USB MOUNTING..."

sed 's/START=12/START=42/' < '/etc/init.d/log' > '/tmp/log.init.sh' && mv '/tmp/log.init.sh' '/etc/init.d/log' && chmod +x '/etc/init.d/log'
/etc/init.d/log disable
/etc/init.d/log enable

echo "ACTIVATE CRON..."

cp /etc/hotspot/etc/crontabs/root /etc/crontabs/ && chmod +x /etc/crontabs/root
/etc/init.d/cron enable

echo "HOSTS: ADDING EMPTY USER BLOCKLIST..."

echo '# user-blocklist-start #' >> /etc/hosts
echo '0.0.0.0 foo' >> /etc/hosts
echo '# user-blocklist-end #' >> /etc/hosts

# USER_BLOCKLIST_START_INDEX=``

# echo "HOSTS: ADDING STEVEN BLACKS BLOCKLIST (https://github.com/StevenBlack/hosts)..."

# wget -O stblho.txt https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts

# echo "### Steven Black Hosts Start ### " >> /etc/hosts
# cat /etc/hotspot/etc/steven-black-hosts.txt >> /etc/hosts
# echo "### Steven Black Hosts End ### " >> /etc/hosts

echo "LUCI BACKUP..."

cp /usr/lib/lua/luci/controller/admin/status.lua /etc/hotspot/bak/ # status controller
cp /usr/lib/lua/luci/view/admin_status/index.htm /etc/hotspot/bak/ # status view

echo "LUCI EXTENSIONS..."

cp /etc/hotspot/usr/lib/lua/luci/view/admin_status/index.tpl.htm /usr/lib/lua/luci/view/admin_status/		# status view
cp /etc/hotspot/usr/lib/lua/luci/view/admin_status/client_domains.htm /usr/lib/lua/luci/view/admin_status/  # domains view
cp /etc/hotspot/usr/lib/lua/luci/controller/admin/status.lua /usr/lib/lua/luci/controller/admin/			# status controller

echo "GENERATING CLIENT LOGS AND LUCI ADMIN FILES..."

/bin/sh /etc/hotspot/clients.sh

echo "FINISHED."

sleep 3

echo "REBOOTING..."

reboot

exit 0

# CLEAR CACHES

# rm -r /tmp/luci-*

# EXECUTE SCRIPT ON REMOTE SERVER

# ssh root@10.0.0.1 'ash -s' < local_script.sh

# HOSTS

# cat /etc/hotspot/hosts.txt >> /etc/hosts
# rm /etc/hotspot/hosts.txt

# DNSMASQ

# echo conf-file=/etc/hotspot/dnsmasq.ads.conf >> /etc/dnsmasq.conf


# herunterladen

# scp -pr root@10.0.0.1:/etc/crontabs/* ./etc/crontabs/

# scp -pr root@10.0.0.1:/mnt/usb/* ./usb/

# rauf laden

# scp -pr ./etc/crontabs/* root@10.0.0.1:/etc/crontabs/

