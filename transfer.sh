#!/bin/sh

scp -p ./logs.sh ./logs.lua root@10.0.0.1:/etc/hotspot
#scp -p ./etc/config/system root@10.0.0.1:/etc/config

#scp -p ./clients.sh root@10.0.0.1:/etc/hotspot
#scp -p ./usr/lib/lua/luci/view/admin_status/domains.htm root@10.0.0.1:/usr/lib/lua/luci/view/admin_status/

#exit 0

#scp -p ./etc/config/dhcp root@10.0.0.1:/etc/config
#scp -p ./etc/config/resolv.conf.custom root@10.0.0.1:/etc/config

exit 0

#
# DOWNLOAD
#


# LOGS
# scp -pr root@10.0.0.1:/mnt/usb/* ./usb
# exit


#
# UPLOAD 
#
# scp ./etc/init.d/mkdir root@10.0.0.1:/etc/init.d/
# scp ./etc/init.d/fsck root@10.0.0.1:/etc/init.d/
# scp ./etc/config/system root@10.0.0.1:/etc/config/

# scp -p logs.sh logs.lua root@10.0.0.1:/etc/hotspot/

# DOWNLOAD LOGS

# scp -pr root@10.0.0.1:/mnt/usb/* ./usb-logs

# scp root@10.0.0.1:/mnt/usb/logs/dhcp-dns.log .
# scp root@10.0.0.1:/mnt/usb/logs/messages .

# scp -p hotspot-v1/client-domains.lua root@10.0.0.1:/etc/hotspot/


# scp -p block-domain.sh unblock-domain.sh root@10.0.0.1:/etc/hotspot/
# exit 0
# scp -p ./hotspot-v1/client-logs.sh root@10.0.0.1:/etc/hotspot/

# scp -p ./hotspot-v1/admin-status.sh root@10.0.0.1:/etc/hotspot/

# LUCI STATUS VIEWS

# scp -p ./hotspot-v1/usr/lib/lua/luci/view/admin_status/index.tpl.htm ./hotspot-v1/usr/lib/lua/luci/view/admin_status/client_domains.htm root@10.0.0.1:/usr/lib/lua/luci/view/admin_status/

# LUCI STATUS CONTROLLER

# BLACKLISTS

# ssh root@10.0.0.1 "mkdir /mnt/usb/hotspot/blacklists"

# scp -p blacklists/*.conf root@10.0.0.1:/mnt/usb/hotspot/blacklists

# DNSMASQ INIT SCRIPT 

# scp -p dnsmasq.init.sh root@10.0.0.1:/etc/init.d/dnsmasq
# ssh root@10.0.0.1 "chmod +x /etc/init.d/dnsmasq"
# ssh root@10.0.0.1 "/etc/init.d/dnsmasq restart"

# exit 0

# scp -p /Users/stef/Router/streetwork/hotspot-dev/usr/lib/lua/luci/view/themes/bootstrap/header.htm root@10.0.0.1:/usr/lib/lua/luci/view/themes/bootstrap

#scp -p ./usr/lib/lua/luci/controller/admin/status.lua root@10.0.0.1:/usr/lib/lua/luci/controller/admin/

# scp -p ./usr/lib/lua/luci/controller/admin/domains.lua root@10.0.0.1:/usr/lib/lua/luci/controller/admin/
#scp -p ./usr/lib/lua/luci/view/admin_status/domains.htm root@10.0.0.1:/usr/lib/lua/luci/view/admin_status/
#scp -p ./usr/lib/lua/luci/view/admin_status/blocklists.htm root@10.0.0.1:/usr/lib/lua/luci/view/admin_status/
scp -p ./usr/lib/lua/luci/view/admin_status/clients.htm root@10.0.0.1:/usr/lib/lua/luci/view/admin_status/
#scp -p ./usr/lib/lua/luci/view/admin_status/client.htm root@10.0.0.1:/usr/lib/lua/luci/view/admin_status/

# DELETE FILE

# ssh root@10.0.0.1 "rm /usr/lib/lua/luci/controller/admin/domains.lua"

# CLEAR LUCI CACHE

ssh root@10.0.0.1 "rm -r /tmp/luci-*"
# ssh root@10.0.0.1 "rm -r /tmp/luci-*"

# GENERATE STATUS VIEWS & CLEAR CACHE

# ssh root@10.0.0.1 "/bin/sh /etc/hotspot/admin-status.sh && rm -r /tmp/luci-*"
