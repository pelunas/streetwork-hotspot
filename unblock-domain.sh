#!/bin/sh

HOSTS_FILE='/etc/hosts'
USER_FILE='/mnt/usb/hotspot/user-blocked-hosts.txt'

HOSTS_TEMP_FILE='/mnt/usb/hotspot/temp-hosts.txt'
USER_TEMP_FILE='/mnt/usb/hotspot/temp-users.txt'

DOMAIN_TO_UNBLOCK="$1"

grep -v "$DOMAIN_TO_UNBLOCK" < "$HOSTS_FILE" > "$HOSTS_TEMP_FILE"
grep -v "$DOMAIN_TO_UNBLOCK" < "$USER_FILE" > "$USER_TEMP_FILE"

# clean up

mv "$HOSTS_TEMP_FILE" "$HOSTS_FILE"
mv "$USER_TEMP_FILE" "$USER_FILE"

exit 0