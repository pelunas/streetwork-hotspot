module("luci.controller.admin.domains", package.seeall)

local domains = {}

function index()
	entry({"admin", "domains"}, alias("admin", "domains", "overview"), _("Domains"), 20).index = true	
end

function main()
	
	local path = '../../../../../../domains'
	domains.get_html(path,1,50)

end

local tld_tpl_start = [===[
<li class="top-domain" onmouseover="show_subs(this)" onmouseout="hide_subs(this)" >
 <table><tr><td class="lc"><a href="http://{{domain}}" target="_blank">{{domain}}</a></td><td><button value="0" onclick="tog(this)">sperren</button></td></tr></table>
 <ul>
]===]

local tld_tpl_end = [===[
 </ul>
</li>
]===]

local sld_tpl = '  <li class="sub-domain"><table><tr><td class="lc"><a href="http://{{domain}}" target="_blank">{{domain}}</a></td><td><button value="0" onclick="tog(this)">sperren</button></td></tr></table></li>\n'


function domains.get_html(path,start_index,num_files)
	
	files = scandir(path)
	-- print(#files)
	local html_str = ''
	local ctr = 1
	local ctr2 = 1

	for i,v in ipairs(files) do

		if ctr >= start_index and ctr < start_index + num_files then
		print(i,v)	
			local tld_html_start = string.gsub(tld_tpl_start,'{{domain}}',v)
			local tld_html_end = tld_tpl_end
			local sld_html_str = ''

			local dfile = io.open(path.. '/' .. v)

			for d in dfile:lines() do
				sld_html_str = sld_html_str .. string.gsub(sld_tpl,'{{domain}}',d)
			end
			
			html_str = html_str .. '<!-- ' .. tostring(ctr2) .. ' --> \n' .. tld_html_start .. sld_html_str .. tld_html_end
			ctr2 = ctr2 + 1
		end
		ctr = ctr + 1
	end
	print(html_str)
end

function scandir(directory)
    -- print('scan dir ' .. directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls "'..directory..'"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

function file_exists(name)
   local f = io.open(name,"r")
   if f ~= nil then io.close(f) return true else return false end
end


function split(str, pat, max, regex)    
    pat = pat or "\n"
    max = max or #str

    local t = {}
    local c = 1

    if #str == 0 then
        return {""}
    end

    if #pat == 0 then
        return nil
    end

    if max == 0 then
        return str
    end

    repeat
        local s, e = str:find(pat, c, not regex)
        max = max - 1
        if s and max < 0 then
            t[#t+1] = str:sub(c)
        else
            t[#t+1] = str:sub(c, s and s - 1)
        end
        c = e and e + 1 or #str + 1
    until not s or max < 0

    return t
end

-- main()