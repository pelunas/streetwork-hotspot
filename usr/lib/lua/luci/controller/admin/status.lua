local io     = require "io"
local os     = require "os"

-- Copyright 2008 Steven Barth <steven@midlink.org>
-- Copyright 2011 Jo-Philipp Wich <jow@openwrt.org>
-- Licensed to the public under the Apache License 2.0.

module("luci.controller.admin.status", package.seeall)

function index()
	
	-- entry(path, target, title=nil, order=nil)

	entry({"admin", "status"}, alias("admin", "status", "overview"), _("Status"), 1).index = true
	entry({"admin", "status", "overview"}, template("admin_status/index"), _("Übersicht"), 1)

	 -- ##### ADDITIONS #### 
 	
 	-- clients 

    entry({"admin", "clients"}, call("action_known_clients"), _("Geräte"), 2).index = true
	entry({"admin", "client"}, call("action_client")).leaf = true
	entry({"admin", "client_xhr"}, call("action_client_xhr")).leaf = true

    -- blacklists

    entry({"admin", "blocklists"}, call("action_blocklists"), _("Blocklisten"), 4).index = true
    entry({"admin", "blacklist_toggle"}, call("action_blacklist_toggle")).leaf = true
	
	-- domains 

	entry({"admin", "domains"}, call("action_domains"), _("Adressen"), 3).index = true
	entry({"admin", "domains_xhr"}, call("action_domains_xhr")).leaf = true
	entry({"admin", "status", "get_blocked_domains_action"}, call("action_get_blocked_domains")).leaf = true
	entry({"admin", "status", "block_domain_action"}, post("action_block_domain")).leaf = true
	entry({"admin", "status", "unblock_domain_action"}, post("action_unblock_domain")).leaf = true

	-- macs

	entry({"admin", "status", "block_mac_action"}, post("action_block_mac")).leaf = true
	entry({"admin", "status", "unblock_mac_action"}, post("action_unblock_mac")).leaf = true
	entry({"admin", "status", "get_blocked_macs_action"}, call("action_get_blocked_macs")).leaf = true

	-- network
	
	entry({"admin", "status", "restart_dnsmasq_action"}, call("action_restart_dnsmasq")).leaf = true
	entry({"admin", "status", "restart_network_action"}, call("action_restart_network")).leaf = true
	
	-- ##### ADDITIONS END ####

	entry({"admin", "status", "iptables"}, template("admin_status/iptables"), _("Firewall"), 3).leaf = true
	entry({"admin", "status", "iptables_action"}, post("action_iptables")).leaf = true

	entry({"admin", "status", "routes"}, template("admin_status/routes"), _("Routes"), 4)
	entry({"admin", "status", "syslog"}, call("action_syslog"), _("System Log"), 5)
	entry({"admin", "status", "dmesg"}, call("action_dmesg"), _("Kernel Log"), 5)
	entry({"admin", "status", "processes"}, cbi("admin_status/processes"), _("Processes"), 6)

	entry({"admin", "status", "realtime"}, alias("admin", "status", "realtime", "load"), _("Realtime Graphs"), 7)

	entry({"admin", "status", "realtime", "load"}, template("admin_status/load"), _("Load"), 1).leaf = true
	entry({"admin", "status", "realtime", "load_status"}, call("action_load")).leaf = true

	entry({"admin", "status", "realtime", "bandwidth"}, template("admin_status/bandwidth"), _("Traffic"), 2).leaf = true
	entry({"admin", "status", "realtime", "bandwidth_status"}, call("action_bandwidth")).leaf = true

	entry({"admin", "status", "realtime", "wireless"}, template("admin_status/wireless"), _("Wireless"), 3).leaf = true
	entry({"admin", "status", "realtime", "wireless_status"}, call("action_wireless")).leaf = true

	entry({"admin", "status", "realtime", "connections"}, template("admin_status/connections"), _("Connections"), 4).leaf = true
	entry({"admin", "status", "realtime", "connections_status"}, call("action_connections")).leaf = true

	entry({"admin", "status", "nameinfo"}, call("action_nameinfo")).leaf = true
end

function action_blacklist_toggle()
	luci.http.prepare_content("application/json")
	local query_str = luci.http.context.request.message.env.QUERY_STRING
	
	-- syslog(query_str)

	if query_str then

		local b = split(query_str,'&')
		local k = b[1]
		local v = b[2]

		local kn = split(k,'-')[1]

		if v == "true" then
			local src = '/mnt/usb/hotspot/blacklists-inbox/'..kn..'.conf'
			local dst = '/mnt/usb/hotspot/blacklists-active/'
			local res = exec('cp '..src..' '..dst)		
		elseif v == "false" then
			local src = '/mnt/usb/hotspot/blacklists-active/'..kn..'.conf'		
			local res = exec('rm '..src)
		end

		-- syslog(res)

	end

	local response = {}	
	response.error = 0
	response.k = k
	response.v = v
	response.res = res
	luci.http.write_json(response)
end



function action_domains_xhr()
	luci.http.prepare_content("application/json")
	local query_str = luci.http.context.request.message.env.QUERY_STRING
	local start_index = tonumber(split(query_str,'=')[2])
	if start_index == nil then start_index = 1 end
	local num_lines = 25
	local path = '/mnt/usb/hotspot/domains'
	local d_obj = get_domains_html(path,start_index,num_lines)
	local html_str = d_obj[1]	
	local response = {}	
	response.error = 0
	response.html = html_str
	luci.http.write_json(response)
end

function action_domains()
	local query_str = luci.http.context.request.message.env.QUERY_STRING
	local start_index = tonumber(split(query_str,'=')[2])
	if start_index == nil then start_index = 1 end
	local num_lines = 25
	local path = '/mnt/usb/hotspot/domains'
	local d_obj = get_domains_html(path,start_index,num_lines)
	local html_str = d_obj[1]
	local filecount = d_obj[2]
	luci.template.render("admin_status/domains", {html_str=html_str,filecount=filecount,start_index=start_index,num_lines=num_lines})
end

function action_client()
	-- <td width="10%">{c}</td>
	local tpl = [[
		<tr>			
			<td>{l}</td>		
		</tr>]]
	local html = ''
	local macaddr = luci.http.context.request.message.env.QUERY_STRING
	local mn = string.gsub(macaddr,':','-')
	local hn = hostname_for_mac(macaddr)
	local idx = 1
	local dir = '/mnt/usb/hotspot/clients/'..mn
	local fc = filecount(dir)
	local filename = filename_in_dir_at_index(dir,idx)
	
	local f = dir..'/'..filename
	local str = ''
	local lines = table_from_file(f)
	table.sort(lines, function(a, b) return a > b end) -- descending

	for i,line in ipairs(lines) do		
		local s = {}
		s['{l}'] = line
		str = render(tpl,s)
		html = html .. str		
	end
	luci.template.render("admin_status/client", {html=html,macaddr=macaddr,host=hn,index=idx,max=fc})
end

function action_client_xhr()
	luci.http.prepare_content("application/json")
	local tpl = [[
		<tr>		
			<td>{l}</td>		
		</tr>]]
	local html = ''
	
	local q = luci.http.context.request.message.env.QUERY_STRING
	local o = split(q,'&')
	local macaddr = o[1]
	local idx = tonumber(o[2])

	local mn = string.gsub(macaddr,':','-')
	local hn = hostname_for_mac(macaddr)	
	local dir = '/mnt/usb/hotspot/clients/'..mn
	local filename = filename_in_dir_at_index(dir,idx)
	
	local f = dir..'/'..filename
	-- syslog(f)
	
	local str = ''
	local lines = table_from_file(f)
	table.sort(lines, function(a, b) return a > b end) -- descending

	for i,line in ipairs(lines) do	
		if line ~= "" or line ~= " " or line ~= "\n" then	
			local s = {}
			-- s['{c}'] = i
			s['{l}'] = line
			str = render(tpl,s)
			html = html .. str
		end
	end
	local response = {}	
	response.error = 0
	response.html = html
	response.index = idx
	luci.http.write_json(response)
	-- luci.template.render("admin_status/client", {html=html,macaddr=macaddr,host=hn,index=idx})
end

function action_known_clients()

	local tpl = [[<tr>
			<td width="10%">
				{c}
			</td>
			<td width="20%">
				<a href="/cgi-bin/luci/admin/client?{t}">{h}</a>
			</td>
			<td width="30%">
				<a href="/cgi-bin/luci/admin/client?{t}">{t}</a>
			</td>
			<td>
				{n}
			</td>
			<td width="15%">    
				<div class="onoffswitch">
	        		<input type="checkbox" onchange="on_switch_change(this)" name="onoffswitch" class="onoffswitch-checkbox" id="{id}" {checked}>
	        		<label class="onoffswitch-label" for="{id}">
	            		<span class="onoffswitch-inner"></span>
	            		<span class="onoffswitch-switch"></span>
	        		</label>
	    		</div>
			</td>
		</tr>]]

	local html = ''
	local leases_db = table_from_file('/mnt/usb/hotspot/dhcp-db.txt')
	
	if leases_db ~= nil then
	    
	    for i,j in ipairs(leases_db) do
	    	local p = split(j,' ')
	    	if #p == 3 then        		        
		        local mac = p[1]
		        local mn = string.gsub(mac,':','-')
		        local ip = p[2]
		        local host = p[3]
		        local checked = ''
		        local fc = filecount('/mnt/usb/hotspot/clients/'..mn)
		        if is_mac_blocked(mac) then checked = 'checked' end
		        -- syslog(mac)
		        -- syslog(tostring(checked))
		        local tpl_1 = string.gsub(tpl,'{t}',mac)
		        local tpl_2 = string.gsub(tpl_1,'{c}',i)
		        local tpl_3 = string.gsub(tpl_2,'{n}',fc)
		        local tpl_4 = string.gsub(tpl_3,'{h}',host)
		        local tpl_5 = string.gsub(tpl_4,'{checked}',checked)
		        local tpl_6 = string.gsub(tpl_5,'{id}',mac)

		        html = html .. tpl_6
		    end
	    end
	end

	luci.template.render("admin_status/clients", {html=html})
end

function action_blocklists()
	local html = ''
	local active_files = scandir('/mnt/usb/hotspot/blacklists-active/')
	local inbox_files = scandir('/mnt/usb/hotspot/blacklists-inbox/')
	local tpl = [[
		<tr>
			<td width="20%">
				{count}
			</td>
			<td>
				{key}
			</td>
			<td width="30%">
				{lc}
			</td>
			<td width="20%">    
				<div class="onoffswitch">
	        		<input type="checkbox" onchange="on_switch_change(this)" name="onoffswitch" class="onoffswitch-checkbox" id="{key}-onoffswitch" {checked}>
	        		<label class="onoffswitch-label" for="{key}-onoffswitch">
	            		<span class="onoffswitch-inner"></span>
	            		<span class="onoffswitch-switch"></span>
	        		</label>
	    		</div>
			</td>
		</tr>
	]]

	local lc = 0
	
	local clean_name, str_key, str_count, str_linecount, str_checked, checked = ''	

	for i,filename in ipairs(inbox_files) do
		if list_contains(active_files,filename) then checked = 'checked' else checked = '' end
		lc = linecount('/mnt/usb/hotspot/blacklists-inbox/'..filename)
		clean_name = split(filename,'.')[1]
		str_key = string.gsub(tpl,'{key}',clean_name)
		str_count = string.gsub(str_key,'{count}',tostring(i))
		str_linecount = string.gsub(str_count,'{lc}',tostring(lc))
		str_checked = string.gsub(str_linecount,'{checked}',checked)


		-- html = html .. str_checked
		html = html .. str_checked

	end


	-- local query_str = luci.http.context.request.message.env.QUERY_STRING
	-- local start_index = tonumber(split(query_str,'=')[2])
	-- if start_index == nil then start_index = 1 end
	-- local num_lines = 25
	-- local path = '/mnt/usb/hotspot/domains'
	-- local d_obj = get_domains_html(path,start_index,num_lines)
	-- local html_str = d_obj[1]
	-- local filecount = d_obj[2]
	luci.template.render("admin_status/blocklists", {html=html})
end


function render(template,subs)
	local tpl = template
	for k,v in pairs(subs) do
		local t = string.gsub(tpl,k,v)
		tpl = t
	end
	return tpl
end

function filename_in_dir_at_index(dir,index)
	return exec_one('ls -r "'..dir..'"|head -'..index..'|tail -1')
end

function table_from_file( file_path )
    local file = io.open(file_path, "r")
    if file == nil then return nil end
    local str = file:read("*all")
    file:close()
    return split(str)
end

function list_contains(table,x)
	for i,v in ipairs(table) do
		if v == x then return true end
	end
	return false
end

function linecount(filename)
    local ctr = 0
    for _ in io.lines(filename) do
        ctr = ctr + 1
    end
    return ctr
end

function filecount(directory)
    local i, popen = 0, io.popen
    local pfile = popen('ls "'..directory..'"')
    for filename in pfile:lines() do
        i = i + 1        
    end
    pfile:close()
    return i
end

function action_restart_network()
	luci.http.prepare_content("application/json")
	local res = exec('/etc/init.d/network restart')
	local response = {}
	response.error = 0
	response.info = res
	luci.http.write_json(response)
end

function action_restart_dnsmasq()
	luci.http.prepare_content("application/json")
	local res = exec('/etc/init.d/dnsmasq restart')
	local response = {}
	response.error = 0
	response.info = res
	luci.http.write_json(response)
end

function is_mac_blocked(mac)
	local b = false
	local uci = require("luci.model.uci").cursor()	
	uci:foreach("wireless", "wifi-iface",
		function(s)
			for key, value in pairs(s) do				
				if key == "maclist" then	
         			for i,v in ipairs(value) do         				
         			 	if v == mac then         			 		
         			 		b = true
         			 	end
         			end           		
				end				
			end
     	end)
	return b
end

function action_toggle_mac()
	luci.http.prepare_content("application/json")
	local macaddr = luci.http.formvalue('macaddr')
	local toggle = luci.http.formvalue('toggle')
	local uci = require("luci.model.uci").cursor()		
	local macs_table = {}
	local found_in_maclist = false
	uci:foreach("wireless", "wifi-iface",
		function(s)
			for key, value in pairs(s) do
				if key == "maclist" then
         			macs_table = value         			
         			for i,v in ipairs(macs_table) do
         				if v == macaddr then found_in_maclist = true end
         			end
         			if toggle == 'true' and not found_in_maclist then table.insert(macs_table,macaddr) end
         			if toggle == 'false' and found_in_maclist then macs_table = remove_from_table(macs_table,macaddr) end
         			uci:set('wireless',s['.name'],'maclist',macs_table)
         			uci:commit("wireless")       		
				end
			end
     	end)
	
	luci.http.write_json({macs=macs_table,error=0})
end

function remove_from_table(table,str)
	local idx = 0
	for i,v in ipairs(table) do
		if v == str then idx = i end
	end
	if idx ~= 0 then
		table.remove(table,idx)
	end
	return table
end

function action_get_blocked_macs_2()

	local uci = require("luci.model.uci").cursor()	
	local has_macfilter = false
	local macs_table = {}

	uci:foreach("wireless", "wifi-iface",
		function(s)
			for key, value in pairs(s) do
				if key == "macfilter" and tostring(value) == "deny" then	
         			has_macfilter = true          		
				end
				if key == "maclist" then	
         			macs_table = value          		
				end
				
			end
     	end)
	return macs_table
end

function action_get_blocked_macs()

	luci.http.prepare_content("application/json")

	local uci = require("luci.model.uci").cursor()
	local response = {error=0}
	local has_wifi = false
	local macs_table = {}

	uci:foreach("wireless", "wifi-iface",
		function(s)
			for key, value in pairs(s) do
				if key == "macfilter" and tostring(value) == "deny" then	
         			has_wifi = true          		
				end
				if key == "maclist" then	
         			macs_table = value          		
				end
				
			end
     	end)

	-- end
	-- 		if uci:get('wireless', 'wifi-iface', 'macfilter') == 'deny' then
	-- 			macs_table = uci:get('wireless', 'wifi-iface', 'maclist')
	-- 			has_wifi = true
	-- 		end
	-- 	end
	
	
	-- local nds_conf_file = '/etc/nodogsplash/nodogsplash.conf'
	-- local config_file = io.open(nds_conf_file, "r")

	if has_wifi == false then 
		response.error = 1
		response.reason = 'libuci-lua could not find wifi interface'
		luci.http.write_json(response)
		return 
	end

	response.macs = macs_table
	luci.http.write_json(response)

end




function action_get_blocked_domains()

	luci.http.prepare_content("application/json")

	local domains_table = {}	
	local response = {error=0}	

	local user_hosts_file_path = '/mnt/usb/hotspot/user-blocked-hosts.txt'
	local user_hosts_file = io.open(user_hosts_file_path, "r")

	if user_hosts_file == nil then
		response.domains = domains_table
		luci.http.write_json(response)
		return
	end 

	for line in user_hosts_file:lines() do
		table.insert(domains_table,line)
	end

	user_hosts_file:close()

	response.domains = domains_table
	luci.http.write_json(response)
end


function action_unblock_domain()

	luci.http.prepare_content("application/json")

	local exit_code = 0
	local domain = luci.http.formvalue('d')

	local user_line = domain
	local hosts_line = '0.0.0.0 ' .. domain

	local user_filename = '/mnt/usb/hotspot/user-blocked-hosts.txt'
	local hosts_filename = '/etc/hosts'

	if not pcall(remove_line_from_file,user_line,user_filename) then exit_code = 1 end
	if not pcall(remove_line_from_file,hosts_line,hosts_filename) then exit_code = 1 end

	luci.http.write_json({error=exit_code})
end

function action_block_domain()

	luci.http.prepare_content("application/json")

	local exit_code = 0
	local domain = luci.http.formvalue('d')
	
	local user_line = domain
	local hosts_line = '0.0.0.0 ' .. domain

	local user_filename = '/mnt/usb/hotspot/user-blocked-hosts.txt'
	local hosts_filename = '/etc/hosts'

	if not pcall(append_line_to_file , user_line , user_filename ) then exit_code = 1 end
	
	if not pcall(append_line_to_file, hosts_line, hosts_filename ) then exit_code = 1 end

	luci.http.write_json({error=exit_code})

end


function get_domains_html(path,start_index,num_files)
	
	local tld_tpl_start = [===[
	<li class="top-domain" onmouseover="show_subs(this)" onmouseout="hide_subs(this)" >
	 <table><tr><td width="25%">{{count}}</td><td class="lc" width="50%"><a href="http://{{domain}}" target="_blank">{{domain}}</a></td><td><button id="{{domain}}" class="cbi-button default small" value="0" onclick="tog(this)">sperren</button></td></tr></table>
	 <ul>
	]===]

	local tld_tpl_end = [===[
	 </ul>
	</li>
	]===]

	local sld_tpl = '  <li class="sub-domain"><table><tr><td width="30%">&nbsp;</td><td class="lc" width="50%"><a href="http://{{domain}}" target="_blank">{{domain}}</a></td><td><button id="{{domain}}" class="cbi-button default small" value="0" onclick="tog(this)">sperren</button></td></tr></table></li>\n'

	-- local files = scandir(path)
	local files = scandir_t(path)
	local filecount = #files
	local html_str = ''
	local ctr = 1
	local ctr2 = 1

	for i,v in ipairs(files) do

		if ctr >= start_index and ctr < start_index + num_files then
		-- print(i,v)	
			local tld_html_start_1 = string.gsub(tld_tpl_start,'{{domain}}',v)
			local tld_html_start = string.gsub(tld_html_start_1,'{{count}}',tostring(ctr))
			local tld_html_end = tld_tpl_end
			local sld_html_str = ''

			local dfile = io.open(path.. '/' .. v)

			for d in dfile:lines() do
				sld_html_str = sld_html_str .. string.gsub(sld_tpl,'{{domain}}',d)
			end
			
			html_str = html_str .. '<!-- ' .. tostring(ctr2) .. ' --> \n' .. tld_html_start .. sld_html_str .. tld_html_end
			ctr2 = ctr2 + 1
		end
		ctr = ctr + 1
	end
	return { html_str, filecount }
end



function action_unblock_mac()

	luci.http.prepare_content("application/json")
	
	local mac_to_unblock = luci.http.formvalue('macaddr')
	local response = {error=1}
	local uci = require("luci.model.uci").cursor()
	local macs_table = {}

	uci:foreach("wireless", "wifi-iface",
		function(s)
			for key, value in pairs(s) do

				if key == "maclist" then	
         			macs_table = value

					-- no blocklist found at all

					if #macs_table == 0 then 
						response.error = 0
						response.info = 'no blocklist found'
						luci.http.write_json(response)		
						return 
					end

					-- is mac in blocklist?

					local index_to_remove = 0
					for i,mac in ipairs(macs_table) do
					  	if mac == mac_to_unblock then
					  		index_to_remove = i
					  	end
					end

					-- mac not found in blocklist

					if index_to_remove == 0 then
						response.error = 0
						response.info = 'mac not found in blocklist'
						response.blocked_macs = macs_table
						luci.http.write_json(response)
						return
					else		
						table.remove(macs_table,index_to_remove)
					end
				
         			if #macs_table == 0 then
         				uci:delete('wireless',s['.name'],'maclist')
         			elseif #macs_table > 0 then
         				uci:set('wireless',s['.name'],'maclist',macs_table)
         			end

         			uci:commit("wireless")
         			response.error = 0
         			response.macs = macs_table       		
				end
				
			end
     	end)	

	luci.http.write_json(response)	
end

function action_block_mac()

	luci.http.prepare_content("application/json")
	
	local new_mac = luci.http.formvalue('macaddr')
	local response = {error=1}
	local uci = require("luci.model.uci").cursor()
	local macs_table = {}

	uci:foreach("wireless", "wifi-iface",		
		function(s)
			local maclist_found = false
			for key, value in pairs(s) do

				if key == "maclist" then
					maclist_found = true
         			macs_table = value
         			table.insert(macs_table,new_mac)
         			uci:set('wireless',s['.name'],'maclist',macs_table)
         			response.error = 0
         			response.macs = macs_table
				end				
			end
			if not maclist_found then
				table.insert(macs_table,new_mac)
				uci:set('wireless',s['.name'],'maclist',macs_table)
				response.error = 0
				response.macs = macs_table
			end
     	end)

	uci:commit("wireless")

	luci.http.write_json(response)	
end




function syslog(str)
	if str == nil then
		os.execute('logger "str = nil"')
	else
		os.execute('logger "'..str..'"')
	end
end






function action_syslog()
	local syslog = luci.sys.syslog()
	luci.template.render("admin_status/syslog", {syslog=syslog})
end

function action_dmesg()
	local dmesg = luci.sys.dmesg()
	luci.template.render("admin_status/dmesg", {dmesg=dmesg})
end

function action_iptables()
	if luci.http.formvalue("zero") then
		if luci.http.formvalue("family") == "6" then
			luci.util.exec("/usr/sbin/ip6tables -Z")
		else
			luci.util.exec("/usr/sbin/iptables -Z")
		end
	elseif luci.http.formvalue("restart") then
		luci.util.exec("/etc/init.d/firewall restart")
	end

	luci.http.redirect(luci.dispatcher.build_url("admin/status/iptables"))
end

function action_bandwidth(iface)
	luci.http.prepare_content("application/json")

	local bwc = io.popen("luci-bwc -i %q 2>/dev/null" % iface)
	if bwc then
		luci.http.write("[")

		while true do
			local ln = bwc:read("*l")
			if not ln then break end
			luci.http.write(ln)
		end

		luci.http.write("]")
		bwc:close()
	end
end

function action_wireless(iface)
	luci.http.prepare_content("application/json")

	local bwc = io.popen("luci-bwc -r %q 2>/dev/null" % iface)
	if bwc then
		luci.http.write("[")

		while true do
			local ln = bwc:read("*l")
			if not ln then break end
			luci.http.write(ln)
		end

		luci.http.write("]")
		bwc:close()
	end
end

function action_load()
	luci.http.prepare_content("application/json")

	local bwc = io.popen("luci-bwc -l 2>/dev/null")
	if bwc then
		luci.http.write("[")

		while true do
			local ln = bwc:read("*l")
			if not ln then break end
			luci.http.write(ln)
		end

		luci.http.write("]")
		bwc:close()
	end
end

function action_connections()
	local sys = require "luci.sys"

	luci.http.prepare_content("application/json")

	luci.http.write("{ connections: ")
	luci.http.write_json(sys.net.conntrack())

	local bwc = io.popen("luci-bwc -c 2>/dev/null")
	if bwc then
		luci.http.write(", statistics: [")

		while true do
			local ln = bwc:read("*l")
			if not ln then break end
			luci.http.write(ln)
		end

		luci.http.write("]")
		bwc:close()
	end

	luci.http.write(" }")
end

function action_nameinfo(...)
	local i
	local rv = { }
	for i = 1, select('#', ...) do
		local addr = select(i, ...)
		local fqdn = nixio.getnameinfo(addr)
		rv[addr] = fqdn or (addr:match(":") and "[%s]" % addr or addr)
	end

	luci.http.prepare_content("application/json")
	luci.http.write_json(rv)
end

function scandir_t(directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls -t "'..directory..'"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end

function scandir(directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls "'..directory..'"')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end


function split(str, pat, max, regex)
	pat = pat or "\n"
	max = max or #str

	local t = {}
	local c = 1

	if #str == 0 then
		return {""}
	end

	if #pat == 0 then
		return nil
	end

	if max == 0 then
		return str
	end

	repeat
		local s, e = str:find(pat, c, not regex)
		max = max - 1
		if s and max < 0 then
			t[#t+1] = str:sub(c)
		else
			t[#t+1] = str:sub(c, s and s - 1)
		end
		c = e and e + 1 or #str + 1
	until not s or max < 0

	return t
end

function exec(command)
	local pp   = io.popen(command)
	local data = pp:read("*a")
	pp:close()
	return data
end

function exec_one(command)
	local pp   = io.popen(command)
	local data = pp:read("*l")
	pp:close()
	return data
end

function string.starts(String,Start)
   return string.sub(String,1,string.len(Start))==Start
end


function append_line_to_file(line,filename)
	local file = io.open(filename,'r+')	
	if file == nil then file = io.open(filename,'a+') end -- Append mode with read mode enabled that opens an existing file or creates a new file.
	local line_found = false
	for l in file:lines() do		
		if l == line then
			line_found = true
		end
	end
	if not line_found then
		file:write(line..'\n')
	end
	file:close()
end

function remove_line_from_file(line,filename)
	local idx = nil
	local ctr = 1	
	local file = io.open(filename,'r')
	for l in file:lines() do 
		if line == l then idx = ctr end
		ctr = ctr + 1
	end

	if idx ~= nil then
		remove_lines_from_file(filename,idx,1)
	end
end

function remove_lines_from_file( filename, starting_line, num_lines )
    
    local fp = io.open( filename, "r" )
    if fp == nil then return nil end
 
    content = {}
    i = 1;
    for line in fp:lines() do
        if i < starting_line or i >= starting_line + num_lines then
            content[#content+1] = line
        end
        i = i + 1
    end
 
    if i > starting_line and i < starting_line + num_lines then
        print( "Warning: Tried to remove lines after EOF." )
    end
 
    fp:close()
    fp = io.open( filename, "w+" )
 
    for i = 1, #content do
        fp:write( string.format( "%s\n", content[i] ) )
    end
 
    fp:close()
end


function hostname_for_mac(macaddr)
	for line in io.lines('/mnt/usb/hotspot/dhcp-db.txt') do
		local v = split(line,' ')
		if v[1] == macaddr then
			return v[3]
		end
	end
	return ''
end