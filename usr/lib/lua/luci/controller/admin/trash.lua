function url_decode (s)
    return s:gsub ('+', ' '):gsub ('%%(%x%x)', function (hex) return string.char (tonumber (hex, 16)) end)
end 

function query_string (url)
    local res = {}
    url = url:match '?(.*)$'
    for name, value in url:gmatch '([^&=]+)=([^&=]+)' do
        value = url_decode (value)
        local key = name:match '%[([^&=]*)%]$'
        if key then
            name, key = url_decode (name:match '^[^[]+'), url_decode (key)
            if type (res [name]) ~= 'table' then
                res [name] = {}
            end
            if key == '' then
                key = #res [name] + 1
            else
                key = tonumber (key) or key
            end
            res [name] [key] = value
        else
            name = url_decode (name)
            res [name] = value
        end
    end
    return res
end


function unblock_domain_action(d, btn){

	var callingButton = btn;
	var callingTableRow = btn.parentElement.parentElement;	

	btn.disabled = true;
	btn.textContent = btn.value = "...";

	var sysauthData = getCookie('sysauth');
	var token = "<%=token%>";
	
	var dataObj = {};
	dataObj.token = token;
	dataObj.sysauth = sysauthData;
	dataObj.d = d;
	
	var encoded = encode(dataObj);
	var http = new XMLHttpRequest();
	var url = "/cgi-bin/luci/admin/status/unblock_domain_action";
	
	http.open("POST", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("token", token );
	http.setRequestHeader("sysauth", sysauthData );
	http.setRequestHeader("d", d );
	http.setRequestHeader("Content-length", encoded.length );		
	http.setRequestHeader("Connection", "close");

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			callingButton.disabled = false;						
			// callingTableRow.style.backgroundColor = "#fff";
			callingButton.value = callingButton.textContent = 'sperren';		
			callingButton.onclick = function(){
				block_domain_action(d,callingButton);
			}
			console.log(http.responseText);
		}
	}

	http.onerror = function(){
		callingButton.disabled = false;
		callingButton.textContent = callingButton.value = "sperren";
		console.log(http.responseText);
	}

	http.send(encoded);		
}



function block_domain_action(d, btn){

	var callingButton = btn;
	var callingTableRow = btn.parentElement.parentElement;	

	callingButton.disabled = true;
	callingButton.textContent = callingButton.value = "...";

	var sysauthData = getCookie('sysauth');
	var token = "<%=token%>";
	
	var dataObj = {};
	dataObj.token = token;
	dataObj.sysauth = sysauthData;
	dataObj.d = d;
	
	var encoded = encode(dataObj);
	var http = new XMLHttpRequest();
	var url = "/cgi-bin/luci/admin/status/block_domain_action";
	
	http.open("POST", url, true);
	http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	http.setRequestHeader("token", token );
	http.setRequestHeader("sysauth", sysauthData );
	http.setRequestHeader("d", d );
	http.setRequestHeader("Content-length", encoded.length );		
	http.setRequestHeader("Connection", "close");

	http.onreadystatechange = function() {//Call a function when the state changes.
		if(http.readyState == 4 && http.status == 200) {
			callingButton.disabled = false;						
			// callingTableRow.style.backgroundColor = "#ffd3b6";
			callingButton.value = callingButton.textContent = 'entsperren';		
			callingButton.onclick = function(){
				unblock_domain_action(d,callingButton);
			}
			console.log(http.responseText);
		}
	}

	http.onerror = function(){
		callingButton.disabled = false;
		callingButton.textContent = callingButton.value = "sperren";
		console.log(http.responseText);
	}

	http.send(encoded);		
}






		<%- if luci.sys.process.info("uid") == 0 and luci.sys.user.getuser("root") and not luci.sys.user.getpasswd("root") then -%>
			<div class="container">
				<div class="alert-message warning">
					<h4><%:No password set!%></h4>
					<%:There is no password set on this router. Please configure a root password to protect the web interface and enable SSH.%><br>
					<a href="<%=pcdata(luci.dispatcher.build_url("admin/system/admin"))%>"><%:Go to password configuration...%></a>
				</div>
			</div>
		<%- end -%>


function action_client_domains()
	local query_str = luci.http.context.request.message.env.QUERY_STRING
	local file_name = split(query_str,'=')[2]
	local base_name = split(file_name,'.')[1]
	local file_path = '/mnt/usb/clients/'..file_name
	luci.template.render("admin_status/client_domains", {file_path=file_path,base_name=base_name})
end

function action_domains()
	local query_str = luci.http.context.request.message.env.QUERY_STRING
	local file_name = split(query_str,'=')[2]
	local base_name = split(file_name,'.')[1]
	local file_path = '/mnt/usb/clients/'..file_name
	luci.template.render("admin_status/client_domains", {file_path=file_path,base_name=base_name})
end


function action_unblock_domain_1()

	luci.http.prepare_content("application/json")

	local domain_to_unblock = luci.http.formvalue('d')
	local exit_code = exec('/bin/sh /etc/hotspot/unblock-domain.sh '..domain_to_unblock)

	luci.http.write_json({error=exit_code})

end

function action_block_domain_1()

	luci.http.prepare_content("application/json")

	local domain_to_block = luci.http.formvalue('d')

	for _ in io.lines(filename) do
	    ctr = ctr + 1
	end

	-- insert domain in user block 
	-- replace user block in hosts file
	-- restart network facility

	local exit_code = exec('/bin/sh /etc/hotspot/block-domain.sh '..domain_to_block)

	luci.http.write_json({error=exit_code})

end





function action_block_mac_nds()	

	local has_error = false
	local successful = false
	local new_mac = luci.http.formvalue('macaddr')

	-- Failed to block MAC 00:21:5c:00:7a:b9.\n
	-- MAC 00:21:5c:00:7a:b9 unblocked.\n
	-- MAC 00:21:5c:00:7a:b9 blocked.\n

	local nds_conf_file = '/etc/nodogsplash/nodogsplash.conf'
	local nds_temp_file = '/etc/nodogsplash/test.conf'


	local now_blocked_msg = "MAC "..new_mac.." blocked.\n"
	local already_blocked_msg = "Failed to block MAC "..new_mac..".\n"

	local ndsctl_msg = exec("ndsctl block "..new_mac)

	os.execute("sleep 1")

	-- local firewall_restart = exec("/etc/init.d/firewall restart")

	local config_file = io.open(nds_conf_file, "r")

	if config_file == nil then 
		-- print('config file not readable, wrong path?')
		-- print(nds_conf_file)
		has_error = true 
	end 

	-- print('reading config file...')

	local str_config_file = config_file:read("*all")	
	local table_config_file = split(str_config_file)
	local new_line = ''
	local new_line_i = 0
	local mac_already_in_list = false
	local found_blocklist_entry = false

	for i,line in ipairs(table_config_file) do
	  	if string.starts(line,"BlockedMACList") then
	  		-- print('found blocklist entry.')
	  		found_blocklist_entry = true
	  		
	  		local bt = split(line," ")  		
	  		local macs_table = split(bt[2],',')

	  		for i,mac in ipairs(macs_table) do
				if mac == new_mac then
					mac_already_in_list = true
					-- print('new mac already in list. not inserting.')
				end
			end

			if not mac_already_in_list then
				table.insert(macs_table,new_mac) 
				-- print('new mac inserted in list.')	  		
			end

	  		new_line = 'BlockedMACList '..table.concat(macs_table,',')
	  		new_line_i = i	  		
	  	end
	end

	config_file:close()

	if mac_already_in_list then
		has_error = false
	end

	-- add or change blocklist value

	if not found_blocklist_entry then
		new_line = 'BlockedMACList '..new_mac
		table.insert(table_config_file,new_line)
	else		
		table_config_file[new_line_i] = new_line
	end

	-- writing table to temp file

	-- print('opening temp file for writing...')

	local temp_file = io.open(nds_temp_file, "w")

	if temp_file == nil then 
		-- print('file not writable, missing permissions?')
		has_error = true
	end 

	for i,line in ipairs(table_config_file) do
		if line ~= '' then
			temp_file:write(line..'\n')
		end
	end

	-- print('temp file written to: '..nds_temp_file)

	temp_file:close()

	-- print('renaming temp to conf...')

	local renamed = os.rename(nds_temp_file,nds_conf_file)

	if not renamed then
		-- print('error on renaming file')		
		has_error = true
	end

	-- print('finished.')
	if ndsctl_msg == now_blocked_msg or ndsctl_msg == already_blocked_msg and not has_error then
		successful = true
	end	

	local a = {successful=successful}

	luci.http.prepare_content("application/json")
	luci.http.write_json(a)
end




function action_unblock_mac_nds()	

	luci.http.prepare_content("application/json")

	local mac_to_unblock = luci.http.formvalue('macaddr')
	local macs_table = {}
	local response = {error=0}	
	local nds_conf_file = '/etc/nodogsplash/nodogsplash.conf'
	local nds_temp_file = '/etc/nodogsplash/test.conf'
	-- Failed to block MAC 00:21:5c:00:7a:b9.\n
	-- MAC 00:21:5c:00:7a:b9 unblocked.\n
	-- MAC 00:21:5c:00:7a:b9 blocked.\n
	-- local now_unblocked_msg = "MAC "..mac_to_unblock.." unblocked.\n"
	-- local already_unblocked_msg = "Failed to unblock MAC "..mac_to_unblock..".\n"

	-- unblock mac instantly

	local ndsctl_msg = exec("ndsctl unblock "..mac_to_unblock)

	-- ndsctl locks config file, wait a second 

	os.execute("sleep 1")

	-- local firewall_restart = exec("/etc/init.d/firewall restart")
	-- local nds_restart = exec("/etc/init.d/nodogsplash restart")	
	
	local config_file = io.open(nds_conf_file, "r")

	-- config file readable?

	if config_file == nil then 
		response.error = 1
		response.reason = 'could not open config file'
		luci.http.write_json(response)
		return 
	end

	-- table of config entries

	local str_config_file = config_file:read("*all")
	local table_config = split(str_config_file)
	local blocklist_index = 0

	-- table of blocklist

	for i,line in ipairs(table_config) do
	  	if string.starts(line,"BlockedMACList") then
	  		blocklist_index = i
			local line_split_space = split(line," ")
			if #line_split_space > 1 then
				if line_split_space[1] == "BlockedMACList" and line_split_space[2] ~= "" then
					macs_table = split(line_split_space[2],',')
				end
			end
	  	end
	end

	config_file:close()

	-- no blocklist found at all

	if #macs_table == 0 then 
		response.error = 0
		response.info = 'no blocklist found.'
		luci.http.write_json(response)		
		return 
	end

	-- is mac in blocklist?

	local index_to_remove = 0
	for i,mac in ipairs(macs_table) do
	  	if mac == mac_to_unblock then
	  		index_to_remove = i
	  	end
	end

	-- mac not found in blocklist

	if index_to_remove == 0 then
		response.error = 0
		response.info = 'mac not found in blocklist.'
		response.blocked_macs = macs_table
		luci.http.write_json(response)
		return
	else		
		table.remove(macs_table,index_to_remove)
	end

	-- rebuild blocklist line and replace it in config table

	local new_blocklist_line = ""

	if #macs_table == 0 then
		new_blocklist_line = ""
	elseif #macs_table == 1 then
		new_blocklist_line = 'BlockedMACList '..macs_table[1]
	else
		new_blocklist_line = 'BlockedMACList '..table.concat(macs_table,',')
	end

	table_config[blocklist_index] = new_blocklist_line

	-- write config table to temp file

	local temp_file = io.open(nds_temp_file, "w")

	if temp_file == nil then 
		response.error = 1
		response.info = 'temp file not writable. permissions? disk space?'
		response.blocked_macs = macs_table
		luci.http.write_json(response)
	end 

	for i,line in ipairs(table_config) do
		if line ~= "" then
			temp_file:write(line..'\n')
		end
	end

	temp_file:close()

	-- rename temp to config 

	local renamed = os.rename(nds_temp_file,nds_conf_file)

	if not renamed then
		response.error = 1
		response.info = 'could not rename temp file to config file.'
		response.blocked_macs = macs_table
		luci.http.write_json(response)	
	end

	-- config file written

	response.error = 0
	response.info = 'finished unblock mac.'
	response.blocked_macs = macs_table
	luci.http.write_json(response)	

end


function action_get_blocked_macs_nds()

	luci.http.prepare_content("application/json")

	local macs_table = {}
	local response = {error=0}	
	local nds_conf_file = '/etc/nodogsplash/nodogsplash.conf'
	local config_file = io.open(nds_conf_file, "r")

	if config_file == nil then 
		response.error = 1
		response.reason = 'could not open config file'
		luci.http.write_json(response)
		return 
	end 	

	local str_config_file = config_file:read("*all")
	local table_config = split(str_config_file)

	for i,line in ipairs(table_config) do
	  	if string.starts(line,"BlockedMACList") then	  			  		
			local line_split_space = split(line," ")
			if #line_split_space > 1 then
				if line_split_space[1] == "BlockedMACList" and line_split_space[2] ~= "" then
					macs_table = split(line_split_space[2],',')
				end
			end
	  	end
	end

	config_file:close()

	response.macs = macs_table
	luci.http.write_json(response)

end