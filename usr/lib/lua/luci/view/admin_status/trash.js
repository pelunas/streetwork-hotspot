
// console.log(e_str);
// var parser = new DOMParser();
// var doc = parser.parseFromString(res, "text/xml");
// container.appendChild(doc);
// next_content_trigger = false;

    function gen_pager() {

        var next_btn = document.getElementById('pager-next-btn');
        var prev_btn = document.getElementById('pager-prev-btn');
        var next_btn_2 = document.getElementById('pager-next-btn-2');
        var prev_btn_2 = document.getElementById('pager-prev-btn-2');

        if (index == 1) {
            prev_btn.disabled = prev_btn_2.disabled = true;
            next_btn.disabled = next_btn_2.disabled = false;
        } else if (index == max) {
            prev_btn.disabled = prev_btn_2.disabled = false;
            next_btn.disabled = next_btn_2.disabled = true;
        } else {
            prev_btn.disabled = prev_btn_2.disabled = false;
            next_btn.disabled = next_btn_2.disabled = false;
        }
    }

	function prev_btn_click () {

		index = index - 1;
		// var f = 1;
		// if(start_index == 1){
		// 	f = 1;
		// }else if(start_index - num_lines == 1){
		// 	f = 1;
		// }else{
		// 	f = start_index - num_lines;
		// }
		// start_index = f
		
		get_client_html();
	}

	function next_btn_click () {
		index = index + 1;
		// if(index == max){}		
		// var f = start_index + num_lines;
		// start_index = f;

		get_client_html();
	}

	function getViewportSize(w) {
	    var w = w || window;
	    if(w.innerWidth != null) return {w:w.innerWidth, h:w.innerHeight};
	    var d = w.document;
	    if (document.compatMode == "CSS1Compat") {
	        return {
	            w: d.documentElement.clientWidth,
	            h: d.documentElement.clientHeight
	        };
	    }
	    return { w: d.body.clientWidth, h: d.body.clientWidth };
	}
	function isViewportVisible(e) {
	    var box = e.getBoundingClientRect();
	    var height = box.height || (box.bottom - box.top);
	    var width = box.width || (box.right - box.left);
	    var viewport = getViewportSize();
	    if(!height || !width) return false;
	    if(box.top > viewport.h || box.bottom < 0) return false;
	    if(box.right < 0 || box.left > viewport.w) return false;
	    return true;    
	}


	function toggle_mac_get (macaddr,toggle) {
		var http = new XMLHttpRequest();
		var url = "/cgi-bin/luci/admin/blacklist_toggle?"+id+'&'+toggle;		
		http.open("GET", url, true);

		http.onreadystatechange = function() {
			
			if(http.readyState == 4 && http.status == 200) {
				console.log(http.status,http.readyState,http.responseText);	
			}
		}

		http.onerror = function(){
			console.log(http.readyState,http.responseText);			
		}
		http.send();
	}


function buildUrl(url, parameters){
  var qs = "";
  for(var key in parameters) {
    var value = parameters[key];
    qs += encodeURIComponent(key) + "=" + encodeURIComponent(value) + "&";
  }
  if (qs.length > 0){
    qs = qs.substring(0, qs.length-1); //chop off last "&"
    url = url + "?" + qs;
  }
  return url;
}




	function qs(search_for) { 
		var query = window.location.search.substring(1); 
		var parms = query.split('&'); 
		for (var i = 0; i < parms.length; i++) { 
			var pos = parms[i].indexOf('='); 
			if (pos > 0 && search_for == parms[i].substring(0, pos)) { 
				return parms[i].substring(pos + 1);; 
			} 
		} 
		return ""; 
	}

	function getCookie(cname) {
    	var name = cname + "=";
    	var ca = document.cookie.split(';');
    	for(var i=0; i<ca.length; i++) {
        	var c = ca[i];
        	while (c.charAt(0)==' ') c = c.substring(1);
        	if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    	}
    	return "";
	}